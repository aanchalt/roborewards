/**
 * Created by 93 on 6/7/17.
 */
const Helper = {

    BaseURL: "https://alpha.roborewards.net/robo/NewUserProfile/",

    //buffelo wild url
    BuffUrl:"ContactLogin?RewardProgramToken=",
    BuffToken:"USNo1wRUjql8MvH1QL8ga024m1J02m",
    BuffUrlSignUp:"RegisterWebFormURLByRPToken?RewardProgramToken=",

    ///Hulu
    HuluUrl:"LoginWebFormURLByRPToken?RewardProgramToken=",
    HuluToken:"USNo1wRUjql8MvH1QL8ga024m1J02m",
    HuluUrlSignUp: "RegisterWebFormURLByRPToken?RewardProgramToken=",

    InitialTheme:"ThemeLayoutByRPToken?RewardProgramToken=",
    InitialToken:"USNo1wRUjql8MvH1QL8ga024m1J02m",

    ForgotPassword:"https://alpha.roborewards.net/robo/NewUserProfile/ForgotPwdLinkByRPToken?RewardProgramToken=",
    ForgotPasswordToken:"USNo1wRUjql8MvH1QL8ga024m1J02m",

    Beacon:"https://alpha.roborewards.net/robo/Beacon/BeaconDataByRPToken?RewardProgramToken=",
    BeaconToken:"USNo1wRUjql8MvH1QL8ga024m1J02m"
};
export default Helper;
