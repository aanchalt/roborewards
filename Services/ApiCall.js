/**
 * Created by 93 on 6/7/17.
 */
import Helper from './Helper'
export function commonAPICall(obj) {
    var strUrl="";
    if(obj.profile=="Buff")
    {
        if (obj.type == "login"){
            strUrl=Helper.BaseURL+Helper.BuffUrl+Helper.BuffToken+"&UserName="+obj.email+"&Password="+obj.pass;
        }else if (obj.type == "signup"){
            strUrl=Helper.BaseURL+Helper.BuffUrlSignUp+Helper.BuffToken;
        }
    }
    if(obj.profile=="Hulu")
    {
        if (obj.type == "login"){
            strUrl=Helper.BaseURL+Helper.HuluUrl+Helper.HuluToken;
        }else if (obj.type == "signup"){
            strUrl=Helper.BaseURL+Helper.HuluUrlSignUp+Helper.HuluToken;
        }

    }
    if(obj.profile=="Initial")
    {
            strUrl=Helper.BaseURL+Helper.InitialTheme+Helper.InitialToken;
    }

    if(obj.type=="ForgotPassword"){
            strUrl=Helper.ForgotPassword+Helper.ForgotPasswordToken;
    }

    if(obj.type == "Beacon"){
        strUrl=Helper.Beacon+Helper.BeaconToken;
    }

    return fetch(strUrl)
        .then((response) => response.json())
        .then((responseJson) => {
            return responseJson;
        })
        .catch((error) => {
            console.error(error);
        });

}
