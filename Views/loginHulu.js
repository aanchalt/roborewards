/**
 * Created by 93 on 6/5/17.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Dimensions,
    View,
    ScrollView,
    TouchableOpacity,
    AsyncStorage,
    TextInput,
    Image,
    Alert,
    StatusBar
} from 'react-native';
import Video from 'react-native-video'
import { StackNavigator} from 'react-navigation'
import {commonAPICall} from '../Services/ApiCall'
const window = Dimensions.get('window');
import Loader from './loader'
import LinearGradient from 'react-native-linear-gradient';

export default class LoginHulu extends Component {
    static navigationOptions = {
        tabBarLabel: 'LoginHulu',
        // Note: By default the icon is only shown on iOS. Search the showIcon option below.

    };
    constructor(props){
debugger;
        super(props);

        this.state={
            isAnimating: false,
            DisplayLogin: false,
            isRemembered: false,
            email:'',
            pass:'',
            backgroundImage: props.theme.BackgroundImageVideo,
            logoImage: props.theme.LogoImage,
            bannerTitleText:props.theme.BannerTitleText,
            bannerDescText:props.theme.BannerDescText,
            bannerDesColor: props.theme.BannerDescColor,
            bannerTitleColor: props.theme.BannerTitleColor,
            bgColor: props.theme.BgColor,
            bgOpacity: props.theme.BgOpacity,
            themeType: props.theme.ThemeType,
            signInText: props.theme.SignInBtnText,
            joinNowText: props.theme.JoinNowBtnText,
            joinNowStartColor: props.theme.JoinNowBtnGradientStrtColor,
            joinNowEndColor: props.theme.JoinNowBtnGradientStopColor,
            signInStartColor: props.theme.SignInBtnGradientStrtColor,
            signInEndColor: props.theme.SignInBtnGradientStopColor,
            headerColor:(props.theme.HeaderColor != "") ? props.theme.HeaderColor : 'black'

        }
debugger
    }

    componentWillMount(){
        debugger;
        let objUser = {
            theme: this.state.themeType
        };
        debugger
        try {
            AsyncStorage.setItem('User', JSON.stringify(objUser));
        } catch (error) {
        }
    }

    setAnimating = (flag) => {
        // Alert.alert(flag + '');
        this.setState((prevState, props) => {
            return {isAnimating:flag};
        });
    };

    validate = () =>
    {
        var flag = false;
        if(this.state.email.length <=0 || this.state.pass.length<=0 )
        {
            this._showAlert("Email Or Password can not be null.");
        }else{

            if(!this.validateEmail(this.state.email)){

                this._showAlert("Enter valid email address.");
            }else{
                flag=true;
            }
        }
        return flag;
    };
    _showAlert(textToMsg){
        Alert.alert('',textToMsg,[{text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false })
    }
    validateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@â€œ]+(\.[^<>()\[\]\\.,;:\s@â€œ]+)*)|(â€œ.+â€œ))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    _onHavingTrouble = () =>{
        var obj = {
            profile:"Hulu",
            type: "ForgotPassword"
        };

        commonAPICall(obj).then((response) => {

                if(response.StatusCode==1)
                {
                    const { navigate } = this.props.navigation;
                    navigate('WebView', {url: response.WebURL,
                        theme: "Buffalo",backGroundImageVideo:this.state.backgroundImage});
                }else {
                }
            }
        ).catch((error) =>{
                console.log(error);
            }
        )
    };


    _onPressSubmit=()=>{

        debugger
        this.setAnimating(true);
        if(this.validate()) {
            commonAPICall({
                email:this.state.email,
                pass:this.state.pass,
                profile:"Buff",
                type: "login"
            }).then((response) => {
                    console.log(response);
                    //    this.setAnimating(false);
                    if(response.StatusCode==1)
                    {
                        let objUser = {
                            username: this.state.email,
                            password: this.state.pass,
                            Remembered: this.state.isRemembered,
                            theme: this.state.themeType
                        };
                        try {
                            AsyncStorage.setItem('User', JSON.stringify(objUser));
                        } catch (error) {
                        }
                        const {navigate} = this.props.navigation;
                        navigate('WebView', {
                            url: response.WebURL,
                            theme: "TGI_Fridays",
                            backGroundImageVideo:this.state.backgroundImage
                        });
                        this.setState({
                            isFromWeb: true
                        })

                    }else {
                        //   this.setAnimating(false);
                        this._showAlert("Check Username and Password.");
                    }
                }
            ).catch((error) =>{
                    // this.setAnimating(false);
                    console.log(error);
                }
            )
        }
        else {
            // this.setAnimating(false);
        }
        // this.props.navigator.push({
        //     name: "second",
        //     title: '',});
    };

    _onPressLogin=()=>{
        this.setState({
            DisplayLogin: true
        });
        this.refs.scrollView.scrollToEnd(true);
    };


    _onPressRememberMe = () =>{
        this.setState({
            isRemembered: !this.state.isRemembered
        })
    };

    _onPressSignUp=()=>{
        var obj = {
            profile:"Hulu",
            type: "signup"
        };
        //this.setAnimating(true);
        commonAPICall(obj).then((response) => {
                this.setAnimating(false);

                if(response.StatusCode==1)
                {

                    let objUser = {
                        theme: this.state.themeType
                    };
                    try {
                        AsyncStorage.setItem('User', JSON.stringify(objUser));
                    } catch (error) {
                    }

                    const { navigate } = this.props.navigation;
                    navigate('WebView', {url: response.WebURL,
                        theme: "Hulu",
                        backGroundImageVideo:this.state.backgroundImage});
                }else {
                }
            }
        ).catch((error) =>{
                //this.setAnimating(false);
                console.log(error);
            }
        )
    };

    validate = () =>
    {
        var flag = false;
        if(this.state.email.length <=0 || this.state.pass.length<=0 )
        {
            this._showAlert("Email Or Password can not be null.");
        }else{

            if(!this.validateEmail(this.state.email)){

                this._showAlert("Enter valid email address.");
            }else{
                flag=true;
            }
        }
        return flag;
    };
    _showAlert(textToMsg){
        Alert.alert('',textToMsg,[{text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false })
    }
    validateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@â€œ]+(\.[^<>()\[\]\\.,;:\s@â€œ]+)*)|(â€œ.+â€œ))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    _onPressSubmit=()=>{
        debugger
        this.setAnimating(true);
        if(this.validate()) {
            commonAPICall({
                email:this.state.email,
                pass:this.state.pass,
                profile:"Buff",
                type: "login"
            }).then((response) => {
                    console.log(response);
                    //    this.setAnimating(false);
                    if(response.StatusCode==1)
                    {
                        let objUser = {
                            username: this.state.email,
                            password: this.state.pass,
                            Remembered: this.state.isRemembered,
                            theme: this.state.themeType
                        };
                        try {
                            AsyncStorage.setItem('User', JSON.stringify(objUser));
                        } catch (error) {
                        }
                        const {navigate} = this.props.navigation;
                        navigate('WebView', {
                            url: response.WebURL,
                            theme: "TGI_Fridays",
                            backGroundImageVideo:this.state.backgroundImage
                        });
                        this.setState({
                            isFromWeb: true
                        })

                    }else {
                        //   this.setAnimating(false);
                        this._showAlert("Check Username and Password.");
                    }
                }
            ).catch((error) =>{
                    // this.setAnimating(false);
                    console.log(error);
                }
            )
        }
        else {
            // this.setAnimating(false);
        }
        // this.props.navigator.push({
        //     name: "second",
        //     title: '',});
    };


    _onHavingTrouble = () =>{
        var obj = {
            profile:"Buff",
            type: "ForgotPassword"
        };

        commonAPICall(obj).then((response) => {

                if(response.StatusCode==1)
                {
                    const { navigate } = this.props.navigation;
                    navigate('WebView', {url: response.WebURL,
                        theme: "Buffalo",backGroundImageVideo:this.state.backgroundImage});
                }else {
                }
            }
        ).catch((error) =>{
                console.log(error);
            }
        )
    };



    render() {
        return (
            <View style={[styles.container,{backgroundColor:(this.state.bgColor != "") ? this.state.bgColor : 'white'}]}>
                <StatusBar
                    backgroundColor={this.state.headerColor}
                    barStyle="default"
                />
                <View style={{flex:1}}>
                    <Video 
                        ref={(ref) => { 
                            this.player = ref 
                        }} 
                        style={{height:window.height,width:window.width}} 
                        source={{uri: this.state.backgroundImage}} 
                        muted={true} 
                        resizeMode="cover"
                        repeat={true} 
                    />

                    <View style={{position: 'absolute',top: 20,left: 0,bottom: 0,right: 0,}}>
                        <View style={{flex:1}}>
                            <ScrollView ref="scrollView"
                                        scrollEnabled={this.state.DisplayLogin}
                                        onContentSizeChange={(contentWidth, contentHeight)=>{_scrollToBottomY = contentHeight; }}>
                               <View style={{height: window.height}}>
                                   <View style={{flex: 2.5,alignItems:'center',marginTop:50,justifyContent:'center',backgroundColor:'transparent'}}>
                                <Text style={{fontFamily: 'Cochin-Bold',fontSize: 40,textAlign: 'center',
    fontWeight: '900',color:(this.state.bannerTitleColor != "") ? this.state.bannerTitleColor : 'black'}}>{this.state.bannerTitleText}</Text>
                                </View>
<View style={{flex: 6}}>
                                <View style={{flex:1,justifyContent:'flex-end'}}>
                                <View style={{padding:15,marginBottom:10,alignItems:'center',justifyContent:'center',backgroundColor:'transparent'}}>
                                    <Text style={{fontFamily: 'Cochin-Bold',fontSize: 30,textAlign: 'center',
    fontWeight: '300',color:(this.state.bannerDesColor != "") ? this.state.bannerDesColor : 'black'}}>{this.state.bannerDescText}</Text>
                                </View>
                            <View style={{flex:7.5,alignItems:'center',marginBottom:10,justifyContent:'center'}}>

                                <TouchableOpacity onPress={()=>this._onPressSignUp()}>



                                        <View style={{backgroundColor:'transparent',borderRadius:3,height:45,alignItems:'center',justifyContent:'center',marginTop:10}}>

                                            <LinearGradient colors={[this.state.joinNowStartColor, this.state.joinNowEndColor]} style={{height:40,width:(window.width)-100,borderRadius:3,alignItems:'center',justifyContent:'center'}}>
                                                <View>
                                                    <Text style={{fontSize: 16,
    fontWeight: '400',color:'white'}}>{this.state.joinNowText}</Text>
                                                </View>
                                            </LinearGradient>

                                        </View>

                                </TouchableOpacity>

                                <TouchableOpacity style={{marginTop:20}} onPress={()=>this._onPressLogin()}>
                                    <View style={{backgroundColor:'transparent',padding:5,borderRadius:5,alignItems:'center',justifyContent:'center'}}>
                                        <Text style={{fontSize: 16,
    fontWeight: '400',color:'white'}}>{this.state.signInText}</Text>
                                    </View>

                                </TouchableOpacity>
                            </View>
                            </View>
</View>
                               </View>
                                {
                                    this.state.DisplayLogin
                                    &&
                                    <View style={{flex:1,height: 340, marginTop: 50,paddingLeft: 50,paddingRight: 50}}>
                                        <View style={{height: 3, borderRadius:3 , backgroundColor: this.state.joinNowStartColor}}/>
                                        <Text style={{fontSize: 21, fontWeight:"700", color: 'white', textAlign: 'center', marginTop: 37,backgroundColor:'transparent'}}>
                                            WELCOME BACK!</Text>
                                        <TextInput
                                            style={{height: 40,borderRadius:3, marginTop: 15,paddingLeft:5,justifyContent:'flex-end',backgroundColor: 'rgba(255,255,255,0.7)'}}
                                            onChangeText={(text) =>  this.setState({email:text})}
                                            placeholder="Email, Mobile or Member ID"
                                            value={this.state.email}
                                            underlineColorAndroid="transparent"
                                        />
                                        <TextInput
                                            style={{height: 40, marginTop: 10,borderRadius:3,paddingLeft:5,justifyContent:'flex-end',backgroundColor: 'rgba(255,255,255,0.7)'}}
                                            onChangeText={(text) =>  this.setState({pass:text})}
                                            placeholder="Password"
                                            value={this.state.pass}
                                            secureTextEntry={true}
                                            underlineColorAndroid="transparent"
                                        />
                                        <View style={{flexDirection:'row',alignItems:'center'}}>
                                            <TouchableOpacity style={{height: 25,width:25, marginTop:10,borderRadius:3,borderColor:  this.state.bannerDesColor,borderWidth: 0.5}}
                                                              onPress={()=>this._onPressRememberMe()}>
                                                <Image style={{height: null,width: null,flex: 1,tintColor:this.state.bannerDesColor}}
                                                       resizeMode='contain'
                                                       source={(this.state.isRemembered)? require('./../Assets/Checked.png'):null}/>
                                            </TouchableOpacity>
                                            <Text style={{marginLeft: 10,marginTop:7,backgroundColor: 'transparent',color:this.state.bannerDesColor}}>Remember me</Text>
                                        </View>

                                        <TouchableOpacity style={{height: 50,borderRadius:3, marginTop: 30,justifyContent:'center',alignItems: 'center',backgroundColor: 'slategray'}}
                                                          onPress={()=>this._onPressSubmit()}>
                                            <Text style={{color:'white',fontSize:16,fontWeight:'500'}}>LOG IN</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity style={{height: 20, marginTop: 10,justifyContent:'center',alignItems: 'center'}} onPress={()=>this._onHavingTrouble()}>
                                            <Text style={{color:'white',fontSize:11,fontWeight:'900',backgroundColor:'transparent',textDecorationLine: 'underline'}}>HAVING TROUBLE LOGGING IN?</Text>
                                        </TouchableOpacity>

                                    </View>
                                    ||
                                    <View style={{flex:1,height: 340, marginTop: 50,paddingLeft: 50,paddingRight: 50}}/>

                                }
                            </ScrollView>
                        </View>
                    </View>

                </View>
            </View>
        );

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },

});