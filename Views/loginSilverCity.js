/**
 * Created by 93 on 6/5/17.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    TouchableOpacity,
    View,
    AsyncStorage,
    StatusBar
} from 'react-native';
import FadeInView from './FadeInView'
import {commonAPICall} from '../Services/ApiCall'
export default class loginSilverCity extends Component {
    constructor(props){
        super(props);
        this.state={
            backgroundImage: props.theme.BackgroundImageVideo,
            logoImage: props.theme.LogoImage,
            bannerDesColor: props.theme.BannerDescColor,
            bannerTitleColor: props.theme.BannerTitleColor,
            bgColor: props.theme.BgColor,
            bgOpacity: props.theme.BgOpacity,
            themeType: props.theme.ThemeType,
            headerColor:(props.theme.HeaderColor != "") ? props.theme.HeaderColor : 'black'
        };
    }

    componentDidMount(){
    }

    componentWillMount(){
        let objUser = {
            theme: this.state.themeType
        };
        try {
            AsyncStorage.setItem('User', JSON.stringify(objUser));
        } catch (error) {
        }
    }
    getNextScreen = () => {
        console.log(this.state.backgroundImage);

        debugger;
        commonAPICall({
            profile:"Hulu",
            type : "signup"
        }).then((response) => {
                console.log(response);
                if(response.StatusCode==1)
                {

                    var that = this;
                    setTimeout(function(){
                        const { navigate } = that.props.navigation;
                        navigate('WebView', {url: response.WebURL,
                            theme: "SilverCity_Rewards",
                            backGroundImageVideo:that.state.backgroundImage});
                    }, 3000);

                }else {
                    this._showAlert("Check Username and Password.");
                }
            }
        ).catch((error) =>
            console.log(response)

        )

    };
    render() {

        return (

            <View style={styles.container}>
                <StatusBar
                    backgroundColor={this.state.headerColor}
                    barStyle="default"
                />
                <FadeInView style={{flex:1,backgroundColor: 'transparent'}}>
                <View style={{opacity:0.7,position: 'absolute',top: 0,left: 0,right: 0,bottom: 0, backgroundColor: 'transparent'}}>
                    <Image style={{height: null,width: null,flex: 1}}
                           source={{uri: this.state.backgroundImage}}
                           resizeMode='cover'
                           onLoadEnd={()=>{
                               setTimeout(this.getNextScreen, 200)
                           }
                           }/>
                </View>
                <View style={{flex:1,marginTop:55,justifyContent:'center',backgroundColor:'transparent'}}>
                    <View style={{flex:2,justifyContent:'center',marginTop:55,alignItems:'center'}}>
                        <View>
                            <Image style={{height:120,width:120,backgroundColor:'transparent'}}
                                   source={{uri: this.state.logoImage}}/>
                        </View>
                    </View>
                    <View style={{flex:2,alignItems:'center',justifyContent:'center'}}>
                        <Image style={{height:70,width:70,backgroundColor:'transparent'}} source={require('../Assets/loadingIndicator.gif')}/>
                    </View>
                    <View style={{flex:2}}>
                    </View>
                </View>
                </FadeInView>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:0,
        backgroundColor: '#F5FCFF',
    },
});