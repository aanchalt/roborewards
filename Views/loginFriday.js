/**
 * Created by 93 on 6/5/17.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    TouchableOpacity,
    View,
    StatusBar,
    ScrollView,
    TextInput,Alert,Dimensions,AsyncStorage
} from 'react-native';

import Loader from './loader'
import {commonAPICall} from '../Services/ApiCall'
import LinearGradient from 'react-native-linear-gradient';
var flag = false;
export default class LoginFriday extends Component {
    constructor(props){
        super(props);
        this.state={
            email:'',
            pass:'',
            DisplayLogin: false,
            isAnimating: false,
            backgroundImage: props.theme.BackgroundImageVideo,
            logoImage: props.theme.LogoImage,
            bannerTitleText:props.theme.BannerTitleText,
            bannerDescText:props.theme.BannerDescText,
            bannerDesColor: props.theme.BannerDescColor,
            bannerTitleColor: props.theme.BannerTitleColor,
            bgColor: props.theme.BgColor,
            bgOpacity: props.theme.BgOpacity,
            themeType: props.theme.ThemeType,
            signInText: props.theme.SignInBtnText,
            forgoetPasswordText: props.theme.ForgetPwdBtnText,
            joinNowText: props.theme.JoinNowBtnText,
            joinNowStartColor: props.theme.JoinNowBtnGradientStrtColor,
            joinNowEndColor: props.theme.JoinNowBtnGradientStopColor,
            forgotpasswordStartColor: props.theme.FrgtPwdBtnGradientStrtColor,
            forgotpasswordEndColor: props.theme.FrgtPwdBtnGradientStopColor,
            signInStartColor: props.theme.SignInBtnGradientStrtColor,
            signInEndColor: props.theme.SignInBtnGradientStopColor,
            headerColor:(props.theme.HeaderColor != "") ? props.theme.HeaderColor : 'black',
            subDescText:props.theme.SubDescText,
            subDescColor:props.theme.SubDescColor,
            isRemembered: false,
            isFromWeb:false
        }
    }
    componentDidMount(){

        try {
            AsyncStorage.getItem('User', (err, data) =>{
                if(data != null){
                    let user = JSON.parse(data);
                    if(user.Remembered){
                        this.setState((prevState,props) => {
                            return {
                                email:user.username,
                                pass:user.password,
                                isRemembered: user.Remembered
                            }
                        });
                    }

                }

                //Alert.alert(data);
            });
        } catch (error) {
            // Alert.alert("Error")
        }
    };


    componentDidUpdate(){

        if(this.state.isFromWeb){
            try {
                AsyncStorage.getItem('User', (err, data) =>{
                    if(data != null){
                        let user = JSON.parse(data);
                        if(user.Remembered){
                            this.setState((prevState,props) => {
                                return {
                                    email:user.username,
                                    pass:user.password,
                                    //isRemembered: user.Remembered
                                }
                            });
                        }else{
                            this.setState((prevState,props) => {
                                return {
                                    email:"",
                                    pass:"",
                                    isFromWeb:false
                                }
                            });
                        }



                    }

                    //Alert.alert(data);
                });
            } catch (error) {
                // Alert.alert("Error")
            }
        }

    }


    validate = () =>
    {
        var flag = false;
        if(this.state.email.length <=0 || this.state.pass.length<=0 )
        {
            this._showAlert("Email Or Password can not be null.");
        }else{

            if(!this.validateEmail(this.state.email)){

                this._showAlert("Enter valid email address.");
            }else{
                flag=true;
            }
        }
        return flag;
    };
    _showAlert(textToMsg){
        Alert.alert('',textToMsg,[{text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false })
    }
    validateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@â€œ]+(\.[^<>()\[\]\\.,;:\s@â€œ]+)*)|(â€œ.+â€œ))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    logInClicked = () => {
        this.setState({
            DisplayLogin: true
        });
        this.refs.scrollView.scrollToEnd(true);
    };
    _onPressSubmit=()=>{
        debugger
        this.setAnimating(true);
        if(this.validate()) {
            commonAPICall({
                email:this.state.email,
                pass:this.state.pass,
                profile:"Buff",
                type: "login"
            }).then((response) => {
                    console.log(response);
                //    this.setAnimating(false);
                    if(response.StatusCode==1)
                    {
                            let objUser = {
                                username: this.state.email,
                                password: this.state.pass,
                                Remembered: this.state.isRemembered,
                                theme: this.state.themeType
                            };
                            try {
                                AsyncStorage.setItem('User', JSON.stringify(objUser));
                            } catch (error) {
                            }
                        const {navigate} = this.props.navigation;
                        navigate('WebView', {
                            url: response.WebURL,
                            theme: "TGI_Fridays",
                            backGroundImageVideo:this.state.backgroundImage
                        });
                        this.setState({
                            isFromWeb: true
                        })

                    }else {
                     //   this.setAnimating(false);
                        this._showAlert("Check Username and Password.");
                    }
                }
            ).catch((error) =>{
                   // this.setAnimating(false);
                    console.log(error);
                }
            )
        }
        else {
           // this.setAnimating(false);
        }
        // this.props.navigator.push({
        //     name: "second",
        //     title: '',});
    };

    _onPressSignUp=()=>{
        this.setAnimating(true);
        commonAPICall({
            profile:"Buff",
            type: "signup"
        }).then((response) => {
          //  this.setAnimating(false);
            debugger
            if(response.StatusCode==1)
                {
                    const { navigate } = this.props.navigation;
                    navigate('WebView', {url: response.WebURL,
                        theme: "TGI_Fridays",backGroundImageVideo:this.state.backgroundImage});
                    // this.props.navigator.push({
                    //     name: "second",
                    //     title: '',
                    //     url: response.WebURL,
                    // });
                }else {
                }
            }
        ).catch((error) =>{
             //   this.setAnimating(false);

            }
        )
    };
    _onPressRememberMe = () =>{
        this.setState({
            isRemembered: !this.state.isRemembered
        })
    };


    _onHavingTrouble = () =>{
        var obj = {
            profile:"Buff",
            type: "ForgotPassword"
        };

        commonAPICall(obj).then((response) => {

                if(response.StatusCode==1)
                {
                    const { navigate } = this.props.navigation;
                    navigate('WebView', {url: response.WebURL,
                        theme: "Buffalo",backGroundImageVideo:this.state.backgroundImage});
                }else {
                }
            }
        ).catch((error) =>{
                console.log(error);
            }
        )
    };

    setAnimating = (flag) => {

        this.setState((prevState, props) => {
            return {isAnimating:flag};
        });
    };

    render() {
        return (
            <View style={[styles.container,{backgroundColor:(this.state.bgColor != "") ? this.state.bgColor : 'white'}]}>
                <StatusBar
                    backgroundColor={this.state.headerColor}
                    barStyle="default"
                />
                <View style={{position: 'absolute',opacity:0.9,top: 0,left: 0,right: 0,bottom: 0, backgroundColor: 'transparent'}}>
                    <Image style={{height: null,width: null,flex: 1}}
                           blurRadius={5}
                           source={{uri: this.state.backgroundImage}}
                           resizeMode='cover'/>
                </View>
                <ScrollView ref="scrollView"
                            scrollEnabled={this.state.DisplayLogin}
                            onContentSizeChange={(contentWidth, contentHeight)=>{
    _scrollToBottomY = contentHeight;
    }}>
                <View style={{flex:4,justifyContent:'center',marginTop:55,alignItems:'center'}}>
                    <View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{color:(this.state.bannerTitleColor != "") ? this.state.bannerTitleColor : 'black',textAlign:'center',fontSize: 40,fontWeight: '400',backgroundColor:'transparent'}}>{this.state.bannerTitleText}</Text>
                    </View>
                    <View style={{flex:3,justifyContent:'center',alignItems:'center',padding:15}}>
                        <View style={{height:2,borderRadius:5,backgroundColor:this.state.joinNowStartColor,width:100,marginBottom:10}}/>
                        <View>
                            <Text style={{color:(this.state.bannerDesColor != "") ? this.state.bannerDesColor : 'black',fontSize:20,fontWeight:'600',textAlign:'center',backgroundColor:'transparent'}}>{this.state.bannerDescText}</Text>

                        </View>
                        <View style={{height:2,borderRadius:5,backgroundColor:this.state.joinNowStartColor,width:100,margin:10,alignItems:'center'}}/>
                        <Text style={{color:(this.state.subDescColor != "") ? this.state.subDescColor : 'black',fontSize:15,fontWeight:'400',textAlign:'center',backgroundColor:'transparent'}}>{this.state.subDescText}</Text>
                    </View>
                    </View>
                </View>
                <View style={{flex:2,alignItems:'center',justifyContent:'center',marginLeft:70,marginRight:70, marginTop: 30}}>

                    <TouchableOpacity onPress={()=>this._onPressSignUp()}>

                        <View style={{backgroundColor:'transparent',width:150,borderRadius:3,height:45,alignItems:'center',justifyContent:'center',marginTop:10}}>

                            <LinearGradient colors={[this.state.joinNowStartColor, this.state.joinNowEndColor]} style={{width:150,height:40,borderRadius:3,alignItems:'center',justifyContent:'center'}}>
                                <View>
                                    <Text style={{color:'white',fontSize:15,fontWeight:'400'}}>{this.state.joinNowText}</Text>
                                </View>
                            </LinearGradient>

                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity  onPress={()=>this.logInClicked()}>

                        <View style={{backgroundColor:'transparent',width:150,borderRadius:3,height:45,alignItems:'center',justifyContent:'center',marginTop:10}}>

                            <LinearGradient colors={[this.state.joinNowStartColor, this.state.joinNowEndColor]} style={{width:150,height:40,borderRadius:3,alignItems:'center',justifyContent:'center'}}>
                                <View>
                                    <Text style={{color:'white',fontSize:15,fontWeight:'400'}}>{this.state.signInText}</Text>
                                </View>
                            </LinearGradient>

                        </View>
                    </TouchableOpacity>
                </View>

                    {
                        this.state.DisplayLogin
                        &&
                        <View style={{flex:1,height: 340, marginTop: 50,paddingLeft: 50,paddingRight: 50}}>
                    <View style={{height: 3, borderRadius:3 , backgroundColor: this.state.joinNowStartColor}}/>
                    <Text style={{fontSize: 21, fontWeight:"700", color: 'white', textAlign: 'center', marginTop: 37,backgroundColor:'transparent'}}>
                        WELCOME BACK!</Text>
                    <TextInput
                        style={{height: 40,borderRadius:3, marginTop: 15,paddingLeft:5,justifyContent:'flex-end',backgroundColor: 'rgba(255,255,255,0.7)'}}
                        onChangeText={(text) =>  this.setState({email:text})}
                        placeholder="Email, Mobile or Member ID"
                        value={this.state.email}
                        underlineColorAndroid="transparent"
                    />
                    <TextInput
                        style={{height: 40, marginTop: 10,borderRadius:3,paddingLeft:5,justifyContent:'flex-end',backgroundColor: 'rgba(255,255,255,0.7)'}}
                        onChangeText={(text) =>  this.setState({pass:text})}
                        placeholder="Password"
                        value={this.state.pass}
                        secureTextEntry={true}
                        underlineColorAndroid="transparent"
                    />
                            <View style={{flexDirection:'row',alignItems:'center'}}>
                                <TouchableOpacity style={{height: 25,width:25, marginTop:10,borderRadius:3,borderColor:  this.state.bannerDesColor,borderWidth: 0.5}}
                                    onPress={()=>this._onPressRememberMe()}>
                                    <Image style={{height: null,width: null,flex: 1,tintColor:this.state.bannerDesColor}}
                                           resizeMode='contain'
                                           source={(this.state.isRemembered)? require('./../Assets/Checked.png'):null}/>
                                </TouchableOpacity>
                                <Text style={{marginLeft: 10,marginTop:7,backgroundColor: 'transparent',color:this.state.bannerDesColor}}>Remember me</Text>
                            </View>


                            <TouchableOpacity  onPress={()=>this._onPressSubmit()}>
                            <View style={{backgroundColor:'transparent',height: 40,borderRadius:3, marginTop: 15,paddingLeft:5,justifyContent:'flex-end',borderRadius:3}}>

                                <LinearGradient colors={[this.state.signInStartColor, this.state.signInEndColor]} style={{flex:1,borderRadius:3,alignItems:'center',justifyContent:'center'}}>
                                    <View>
                                        <Text style={{color:'white',fontSize:15,fontWeight:'400'}}>LOG IN</Text>
                                    </View>
                                </LinearGradient>

                            </View>
                            </TouchableOpacity>


                    <TouchableOpacity style={{height: 20, marginTop: 10,justifyContent:'center',alignItems: 'center'}} onPress={()=>this._onHavingTrouble()}>
                        <Text style={{color:'white',fontSize:11,fontWeight:'900',backgroundColor:'transparent',textDecorationLine: 'underline'}}>HAVING TROUBLE LOGGING IN?</Text>
                    </TouchableOpacity>

            </View>
                        ||
                        <View style={{flex:1,height: 340, marginTop: 50,paddingLeft: 50,paddingRight: 50}}/>

    }
            </ScrollView>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
});