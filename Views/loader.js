/**
 * Created by LaNet on 4/27/17.
 */
import React, { Component } from 'react';
import { ActivityIndicator,
    View, Image,
    Dimensions}
    from 'react-native';

class Loader extends Component{

    constructor(props){
        super(props);
        this.state = {
            bgImage:props.backGroundImage,
        }
        debugger

    }

    render(){
        return( (this.props.animating) ?
                <View style={{ position:'absolute', backgroundColor: 'rgb(1,26,49)',
                    height: Dimensions.get('window').height, width:Dimensions.get('window').width,
                    alignItems:'center', justifyContent:'center'}}>
                     <Image style={{height:  Dimensions.get('window').height,width: Dimensions.get('window').width,flex: 1 ,alignItems:'center', justifyContent:'center'}}
                     source={{uri: this.state.bgImage}}
                     resizeMode='cover'>
                         <Image style={{height: 50,width:50,alignSelf: 'center'}}
                        source={require('./../Assets/830.gif')}/>
                     </Image>

                </View>
                : null
        );
    }
}

module.exports = Loader;

