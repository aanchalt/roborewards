/**
 * Created by 93 on 6/6/17.
 */
/**
 * Created by 93 on 6/5/17.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    Alert,
    TextInput,
    TouchableOpacity,
    View,
    AsyncStorage,
    Dimensions,
    StatusBar
} from 'react-native';
import {commonAPICall} from '../Services/ApiCall'
import Loader from './loader'
import LinearGradient from 'react-native-linear-gradient';

export default class LoginBuffalo extends Component {
    constructor(props){
        super(props);
        this.state={
            email:'',
            pass:'',
            isAnimating: false,
            backgroundImage: props.theme.BackgroundImageVideo,
            logoImage: props.theme.LogoImage,
            bannerTitleText:props.theme.BannerTitleText,
            bannerDescText:props.theme.BannerDescText,
            bannerDesColor: props.theme.BannerDescColor,
            bannerTitleColor: props.theme.BannerTitleColor,
            bgColor: props.theme.BgColor,
            bgOpacity: props.theme.BgOpacity,
            themeType: props.theme.ThemeType,
            signInText: props.theme.SignInBtnText,
            forgoetPasswordText: props.theme.ForgetPwdBtnText,
            joinNowText: props.theme.JoinNowBtnText,
            joinNowStartColor: props.theme.JoinNowBtnGradientStrtColor,
            joinNowEndColor: props.theme.JoinNowBtnGradientStopColor,
            forgotpasswordStartColor: props.theme.FrgtPwdBtnGradientStrtColor,
            forgotpasswordEndColor: props.theme.FrgtPwdBtnGradientStopColor,
            signInStartColor: props.theme.SignInBtnGradientStrtColor,
            signInEndColor: props.theme.SignInBtnGradientStopColor,
            headerColor:(props.theme.HeaderColor != "") ? props.theme.HeaderColor : 'black',
            isRemembered: false,
            isFromWeb:false
        }
    }
    componentWillMount(){

        try {
            AsyncStorage.getItem('User', (err, data) =>{
                if(data != null){
                    let user = JSON.parse(data);
                    if(user.Remembered){
                        debugger
                        this.setState((prevState,props) => {
                            return {
                                email:user.username,
                                pass:user.password,
                                isRemembered: user.Remembered
                            }
                        });
                    }else{
                        this.setState((prevState,props) => {
                            return {
                                email:"",
                                pass:"",
                                isRemembered:false
                            }
                        });
                    }


                }

                //Alert.alert(data);
            });
        } catch (error) {
            // Alert.alert("Error")
        }
    };


    componentDidUpdate(){

        if(this.state.isFromWeb){
            try {
                AsyncStorage.getItem('User', (err, data) =>{
                    if(data != null){
                        let user = JSON.parse(data);
                        if(user.Remembered){
                            this.setState((prevState,props) => {
                                return{
                                    email:user.username,
                                    pass:user.password,
                                    //isRemembered: user.Remembered
                                }
                            });
                        }else{
                            this.setState((prevState,props) => {
                                return {
                                    email:"",
                                    pass:"",
                                    isFromWeb:false
                                }
                            });
                        }

                    }

                    //Alert.alert(data);
                });
            } catch (error) {
                // Alert.alert("Error")
            }
        }

    }

    setAnimating = (flag) => {
        // Alert.alert(flag + '');
        this.setState((prevState, props) => {
            return {isAnimating:flag};
        });
    };

    validate = () =>
    {
        var flag = false;
        if(this.state.email.length <=0 || this.state.pass.length<=0 )
        {
            this._showAlert("Email Or Password can not be null.");
        }else{

            if(!this.validateEmail(this.state.email)){

                this._showAlert("Enter valid email address.");
            }else{
                flag=true;
            }
        }
        return flag;
    }
    _showAlert(textToMsg){
        Alert.alert('',textToMsg,[{text: 'OK', onPress: () => console.log('OK Pressed')},],{ cancelable: false })
    }
    validateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@â€œ]+(\.[^<>()\[\]\\.,;:\s@â€œ]+)*)|(â€œ.+â€œ))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    _onPressSubmit=()=>{
        debugger
        var obj = {
            email:this.state.email,
            pass:this.state.pass,
            profile:"Buff",
            type: "login"
        };

      //  this.setAnimating(true);

        if(this.validate()) {
            commonAPICall(obj).then((response) => {
                debugger;
                    console.log(response);
               // this.setAnimating(false);

                    if(response.StatusCode==1)
                    {
                            let objUser = {
                                username: this.state.email,
                                password: this.state.pass,
                                Remembered: this.state.isRemembered,
                                theme: this.state.themeType
                            };
                            try {
                                AsyncStorage.setItem('User', JSON.stringify(objUser));
                            } catch (error) {
                            }

                        const {navigate} = this.props.navigation;
                        navigate('WebView', {
                            url: response.WebURL,
                            theme: "Buffalo",
                            backGroundImageVideo:this.state.backgroundImage
                        });
                        this.setState({
                            isFromWeb: true
                        })

                    }else {
                        this._showAlert("Check Username and Password.");
                    }
                }
            ).catch((error) =>{
                   // this.setAnimating(false);
                    console.log(error);
            }
            )
        }else{
           // this.setAnimating(false);
        }

    };
    _onPressSignUp=()=>{
        var obj = {
            profile:"Buff",
            type: "signup"
        };
        this.setAnimating(true);
            commonAPICall(obj).then((response) => {
             //   this.setAnimating(false);

                    if(response.StatusCode==1)
                    {
                        console.log("SignUP://////",response.WebURL);

                        const { navigate } = this.props.navigation;
                        navigate('WebView', {url: response.WebURL,
                            theme: "Buffalo",backGroundImageVideo:this.state.backgroundImage});
                    }else {
                    }
                }
            ).catch((error) =>{
                  //  this.setAnimating(false);
                    console.log(error);
                }
            )
    };
    _onPressRememberMe = () =>{
        this.setState({
            isRemembered: !this.state.isRemembered
        })

    };

    _onForgotPassword = () =>{
        var obj = {
            profile:"Buff",
            type: "ForgotPassword"
        };

        commonAPICall(obj).then((response) => {

                if(response.StatusCode==1)
                {
                    const { navigate } = this.props.navigation;
                    navigate('WebView', {url: response.WebURL,
                        theme: "Buffalo",backGroundImageVideo:this.state.backgroundImage});
                }else {
                }
            }
        ).catch((error) =>{
                console.log(error);
            }
        )
    };




    render() {
        return (

            <View style={[styles.container,{backgroundColor:(this.state.bgColor != "") ? this.state.bgColor : 'white'}]}>
                <StatusBar
                    backgroundColor={this.state.headerColor}
                    barStyle="default"
                />
                <View style={{position: 'absolute',opacity:0.9,top: 0,left: 0,right: 0,bottom: 0, backgroundColor: 'transparent'}}>
                    <Image style={{height: null,width: null,flex: 1, }}
                           source={{uri: this.state.backgroundImage}}
                           resizeMode='cover'/>
                </View>
                <View style={{justifyContent:'center',marginTop:30,alignItems:'center'}}>
                    <View>
                        <View style={{justifyContent:'flex-start',alignItems:'center'}}>
                            <Text style={{color:(this.state.bannerTitleColor != "") ? this.state.bannerTitleColor : 'black',textAlign:'center',fontSize: 30,fontWeight: '400',fontFamily: 'Aachen',backgroundColor:'transparent'}}>{this.state.bannerTitleText}</Text>
                            <Image style={{height:100,width:65,marginTop:20}} resizeMode='contain' source={{uri: this.state.logoImage}}/>
                        </View>
                    </View>
                </View>

                <View style={{padding:25}}>
                    <View style={{marginTop:5,padding:5}}>
                        <View style={{flexDirection:'row',borderRadius:2,height: 40,backgroundColor: 'rgba(255,255,255,0.7)'}}>
                            <TextInput
                                style={{flex:1,justifyContent:'flex-end',fontFamily: 'Aachen',paddingLeft:5}}
                                onChangeText={(text) => this.setState({email:text})}
                                placeholder="Email"
                                value={this.state.email}
                                underlineColorAndroid="transparent"
                            />

                        </View>
                        <View style={{height:1,backgroundColor:'black'}}/>

                    </View>
                    <View style={{padding:5}}>
                        <View style={{flexDirection:'row',borderRadius:2,height: 40,backgroundColor: 'rgba(255,255,255,0.7)'}}>
                            <TextInput
                                style={{flex:1,justifyContent:'flex-end',fontFamily: 'Aachen',paddingLeft:5}}
                                onChangeText={(text) =>  this.setState({pass:text.toString()})}
                                placeholder="Password"
                                value={this.state.pass}
                                secureTextEntry={true}
                                underlineColorAndroid="transparent"
                            />
                        </View>
                        <View style={{height:1,backgroundColor:'black'}}/>
                    </View>
                    <View style={{flexDirection:'row',alignItems:'center',padding:5}}>
                        <TouchableOpacity style={{height: 25,width:25, marginTop:10,borderRadius:2,borderColor: this.state.bannerDesColor,borderWidth: 0.5}}
                                          onPress={()=>this._onPressRememberMe()}>
                        <Image style={{height: null,width: null,flex: 1,tintColor:this.state.bannerDesColor}} resizeMode='contain'
                        source={(this.state.isRemembered)? require('./../Assets/Checked.png'):null}/>
                        </TouchableOpacity>
                        <Text style={{marginLeft: 10,marginTop:7,backgroundColor:'transparent',color: this.state.bannerDesColor}}>Remember me</Text>
                    </View>
                    <View style={{justifyContent:'center',marginTop:10}}>
                        <TouchableOpacity onPress={()=>this._onPressSubmit()}>
                            <LinearGradient colors={[this.state.signInStartColor, this.state.signInEndColor]} style={{margin:5,height:40,borderRadius:2,alignItems:'center',justifyContent:'center'}}>

                            <View>
                                <Text style={{backgroundColor:'transparent',color:'#9A9A99',fontSize:16,fontWeight:'600',fontFamily: 'Aachen'}}>{this.state.signInText}</Text>
                            </View>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                    <View style={{justifyContent:'center',marginTop:0}}>

                        <TouchableOpacity onPress={()=>this._onForgotPassword()}>
                            <LinearGradient colors={[this.state.forgotpasswordStartColor, this.state.forgotpasswordEndColor]} style={{margin:5,height:40,borderRadius:2,alignItems:'center',justifyContent:'center'}}>

                                <View>
                                    <Text style={{backgroundColor:'transparent',color:'#9A9A99',fontSize:16,fontWeight:'600',fontFamily: 'Aachen'}}>{this.state.forgoetPasswordText}</Text>
                                </View>
                            </LinearGradient>
                        </TouchableOpacity>

                    </View>
                    <View style={{height:2,borderRadius:5,backgroundColor:'#E1E1E1',margin:5}}/>
                    <View style={{justifyContent:'center',marginTop:0}}>

                        <TouchableOpacity onPress={()=>this._onPressSignUp()}>
                            <LinearGradient colors={[this.state.joinNowStartColor, this.state.joinNowEndColor]} style={{margin:5,height:40,borderRadius:2,alignItems:'center',justifyContent:'center'}}>

                                <View>
                                    <Text style={{backgroundColor:'transparent',color:'#9A9A99',fontSize:16,fontWeight:'600',fontFamily: 'Aachen'}}>{this.state.joinNowText}</Text>
                                </View>
                            </LinearGradient>
                        </TouchableOpacity>

                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
});