
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    StatusBar,
    AsyncStorage,
    DeviceEventEmitter,
    Image
} from 'react-native';
import LoginHulu from './loginHulu'
import LoginFriday from './loginFriday'
import Webview from './Webview'
import LoginBuffalo from './loginBuffalo'
import LoginJohn from './loginJohn'
import LoginSilverCity from './loginSilverCity'
//const layoutData = require('./../app.json');
import {commonAPICall} from '../Services/ApiCall'
var themeNew = "";
import Helper from './../Services/Helper'
var count=0;

export default class App extends Component {
    constructor(props){
        super();
        this.state={
            theme: "",
            isDirectWeb:false,
            backGroundImageVideo: "",
            username:''
        }
    }

    componentWillMount() {

        DeviceEventEmitter.addListener('your listener', (e)=>{
            this.setState({
                isDirectWeb: true
            });
        });

        var obj = {
            profile:"Initial",
        };
        commonAPICall(obj).then((response) => {
            debugger
                if(response.StatusCode==1)
                {
                    themeNew = response;
                    debugger
                    this.setState({
                        backGroundImageVideo:themeNew.BackgroundImageVideo
                    })
                    AsyncStorage.getItem('User', (err, result) => {
                        debugger
                        if (result != null) {
                            let user = JSON.parse(result);
                            if (user.theme != themeNew.ThemeType) {
                                let objUser = {
                                    email: "",
                                    pass:"",
                                    isRemembered:false
                                };
                                try {
                                    AsyncStorage.setItem('User', JSON.stringify(objUser));
                                } catch (error) {
                                }
                                this.setState({
                                    isDirectWeb: true
                                });

                            }else if(user.Remembered){
                                this.getWebView();
                            }else if(user.theme == "Hulu"){
                                this.setState({
                                    isDirectWeb:true
                                });
                            } else{
                                AsyncStorage.getItem('lastURL', (err, result1) => {
                                    if (result1 != "https://alpha.roborewards.net/UserProfile/Login.aspx?Action=Logout"){
                                        this.getWebView();
                                    }else{
                                        this.setState({
                                            isDirectWeb:true
                                        });

                                    }
                                });
                            }
                        }else{
                                debugger
                                AsyncStorage.getItem('lastURL', (err, result1) => {
                                    if (result1 != "https://alpha.roborewards.net/UserProfile/Login.aspx?Action=Logout"){
                                        this.getWebView();
                                    }else{
                                        this.setState({
                                            isDirectWeb:true
                                        });

                                    }
                                });

                            }



                    });

                }else {
                    this._showAlert("Check Username and Password.");
                }
            }
        ).catch((error) =>
            console.log(error)
        );

        // try {
        //     AsyncStorage.setItem('User', JSON.stringify(layoutData));
        // } catch (error) {
        // }
        // AsyncStorage.getItem('User', (err, data) =>{
        //     if(data != null) {
        //         console.log(data);
        //         debugger;
        //     }
        // });

    }

    getTheme = () => {
debugger
        if (this.state.isDirectWeb) {
            if (themeNew.ThemeType != "") {
                if (themeNew.ThemeType == "Buffalo") {
                    return (
                        <LoginBuffalo
                            theme={themeNew}
                            navigation={this.props.navigation}
                        />
                    )

                } else if (themeNew.ThemeType == "TGI_Fridays") {
                    return (
                        <LoginFriday
                            theme={themeNew}
                            navigation={this.props.navigation}
                        />
                    )

                } else if (themeNew.ThemeType == "Hulu") {
                    return (
                        <LoginHulu
                            theme={themeNew}
                            navigation={this.props.navigation}
                        />
                    )

                } else if (themeNew.ThemeType == "Papa_Jones") {
                    return (
                        <LoginJohn
                            theme={themeNew}
                            navigation={this.props.navigation}
                        />
                    )

                } else if (themeNew.ThemeType == "SilverCity_Rewards") {
                    return (
                        <LoginSilverCity
                            theme={themeNew}
                            navigation={this.props.navigation}
                        />
                    )
                }
            }
        }else {
            return (
                <View style={{flex: 1,justifyContent: 'center', backgroundColor:'rgb(1,26,49)'}}>
                    <Image style={{height: 50,width:50,alignSelf: 'center'}}
                        source={require('./../Assets/830.gif')}/>
                </View>
            )
        }
    };
    getWebView = () => {

        if (themeNew.ThemeType == "Buffalo"|| themeNew.ThemeType == "TGI_Fridays" || themeNew.ThemeType == "Hulu"){
            AsyncStorage.getItem('User', (err, result) => {
                    if (result != null){
                        let user = JSON.parse(result);
                        this.getLogin({
                            email:user.username,
                            pass:user.password,
                            profile:"Buff",
                            type: "login"
                        });
                    }else{
                        debugger;
                        this.setState({
                            isDirectWeb : true
                        })
                    }

                });
        }else{
            // AsyncStorage.getItem('lastURL', (err, result1) => {
            //     if (result1 != "https://alpha.roborewards.net/UserProfile/Login.aspx?Action=Logout"){
            //         this.getLogin({
            //             profile:"Hulu",
            //             type: "login"
            //         });
            //     }else{
                    this.setState({
                        isDirectWeb : true
                    })
            //     }
            // });
            }

    };
    getLogin = (credentials) => {

            commonAPICall(credentials).then((response) => {

                    if(response.StatusCode==1)
                    {
                                const { navigate } = this.props.navigation;
                                debugger;
                                navigate('WebView', {url: response.WebURL,
                                    themeType: themeNew.ThemeType,theme:themeNew,backGroundImageVideo:this.state.backGroundImageVideo});
                    }else {
                    }
                }
            ).catch((error) =>{
                }
            )
    };
    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    hidden={false}
                />
                {
                    this.getTheme()
                }
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

        backgroundColor: '#F5FCFF',
    },
});

