/**
 * Created by 93 on 6/5/17.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Dimensions,
    View,
    WebView,
    Alert,
    ScrollView,
    TouchableOpacity,
    AsyncStorage,
    DeviceEventEmitter,
    BackHandler,
    Platform,
    ToastAndroid
} from 'react-native';
import Loader from './loader'

import Beacons from 'react-native-beacons-manager';
import signalr from 'react-native-signalr';
import Moment from 'moment';
import FCM from "react-native-fcm";

import { BluetoothStatus } from 'react-native-bluetooth-status';
import {commonAPICall} from '../Services/ApiCall';
import Helper from '../Services/Helper';

const window = Dimensions.get('window');

let flag=true;
let isBackPressed=false
let intime;
let outtime;
let BeaconData = [];
let welcomeMsg = false; // for welcomeMessage
let Beacon;
let delay = false; // for TimeDelay message
let connectionStart = false; //for restric recursive calling  of creatConnectionHub
let isHomePage = false;



export default class Webview extends Component {
    constructor(props){
        super(props);
        const {state} = this.props.navigation;
        this.state={
            isAnimating: false,
            themeType: state.params.theme,
            bgImage: state.params.backGroundImageVideo,
            url: state.params.url,
            isfromWeb:false,
            newSession:true,
            token: "",
            tokenCopyFeedback: "",
            timer:false,
            identifier: '326b1e18a580751a157fe38fd194052d',
            uuid: 'B9407F30-F5F8-466E-AFF9-25556B57FE6D',
            Major:0,
            Minor:0,
            contacId:"",
            welcomeMsg:'Welcome',
            delayTime:"",
            checkInTime:"",
            sessionDuration:2,
            bluetoothState: '',
            sessionStarted:false
        }
    }


    setAnimating = (flag) => {
        // Alert.alert(flag + '');
        this.setState((prevState, props) => {
            return {isAnimating:flag};
        });
    };


    showLocalNotification() {
        console.log("In Function")
        FCM.presentLocalNotification({
            vibrate: 500,
            title: 'RoboRewards',
            body: this.state.welcomeMsg,
            priority: "high",
            show_in_foreground: true,
            picture: 'https://firebase.google.com/_static/af7ae4b3fc/images/firebase/lockup.png'
        })
    }


    getBeaconData(){

        let obj = {
            type:"Beacon",
        };

        commonAPICall(obj).then((response) => {
                if(response.StatusCode==1)
                {
                    BeaconData = response.Response

                }else {
                    this._showAlert("Something went wrong.");
                }
            }
        ).catch((error) =>
            console.log(error)
        );

    }


    newSession() {

        try {
            return this.checkSession().then(() => {
                return Promise.resolve(true)
            });
        } catch (error) {

        }
    }



    checkSession(){
        Moment.locale('en');
        let time = new Date();
        let obj = {
            RegionT:'true',
            RegionI:time.toString()
        }

        try{
            return AsyncStorage.getItem('Session', (err, result1) => {

                session = JSON.parse(result1)
                if(session != null){
                    stime = new Date(session.RegionI)
                    sessionTime = Moment(stime)

                    let delay = Moment.duration(this.state.checkInTime, "HH:mm").asMinutes();
                    let diff = Moment.utc(Moment(time, "HH:mm:ss").diff(Moment(sessionTime, "HH:mm:ss"))).format("mm")

                    if(parseInt(diff) > delay){

                        return AsyncStorage.setItem('newSession', JSON.stringify(true),(err) => {
                            return AsyncStorage.setItem('Session', "",(err) => {
                                return AsyncStorage.setItem('connectionStart', JSON.stringify(false),(err) => {
                                    this.setState({timer: false})
                                    return Promise.resolve(true)
                                });
                                return Promise.resolve(true)
                            });
                            return Promise.resolve(true)
                        });
                    }
                }
                else if(session == null){
                    return AsyncStorage.setItem('Session', JSON.stringify(obj),(err) => {
                        return Promise.resolve(true)
                    });
                }



            });
        }catch (error){

        }
    }



    createHubConnection(){

        Beacon = {
            UUID:this.state.uuid,
            Major:this.state.Major,
            Minor:this.state.Minor,
            ContactID:this.state.contacId,
            Time:new Date(),
            RewardProgramID:Helper.InitialToken
        }

        const connection = signalr.hubConnection('http://alpha.roborewards.net:56523/signalr');
        connection.logging = true;

        const proxy = connection.createHubProxy('signalRChatHub');
        //receives broadcast messages from a hub function, called "helloApp"
        proxy.on('receiveMessage', (argOne, argTwo, argThree, argFour) => {
            console.log('message-from-server', argOne, argTwo, argThree, argFour);
            //Here I could response by calling something else on the server...
        });

        // atempt connection, and handle errors
        connection.start().done(() => {
            debugger
            console.log('Now connected, connection ID=' + connection.id);
            proxy.invoke('broadCastMessage', 'aanchal', JSON.stringify(Beacon))
                .done((directResponse) => {
                    console.log('direct-response-from-server', directResponse);
                }).fail((error) => {
                console.log('Something went wrong when calling server, it might not be up and running?')
            });

        }).fail((e) => {
            console.log('Failed'+ e);
        });

        //connection-handling
        connection.connectionSlow(() => {
            console.log('We are currently experiencing difficulties with the connection.')
        });

        connection.error((error) => {
            const errorMessage = error.message;
            let detailedError = '';
            if (error.source && error.source._response) {
                detailedError = error.source._response;
            }
            if (detailedError === 'An SSL error has occurred and a secure connection to the server cannot be made.') {
                console.log('When using react-native-signalr on ios with http remember to enable http in App Transport Security https://github.com/olofd/react-native-signalr/issues/14')
            }
            console.log('SignalR error: ' + errorMessage, detailedError)
        });


    }


    beaconConfig(){

        this.getBeaconData()
        const region = {
            identifier: "326b1e18a580751a157fe38fd194052d",
            uuid: this.state.uuid,
            Major: this.state.Major,
            Minor: this.state.Minor
        };


        if(Platform.OS === 'ios'){

            DeviceEventEmitter.addListener(
                'authorizationStatusDidChange',
                (info) => console.log('authorizationStatusDidChange: ', info)
            );

            Beacons.requestAlwaysAuthorization();

            Beacons.startMonitoringForRegion(region);
            Beacons.startRangingBeaconsInRegion(region);
            Beacons.startUpdatingLocation();

        }else{

            // Range for beacons inside the region
            Beacons.detectIBeacons();
            Beacons.setBackgroundScanPeriod(10000);

            Beacons.startRangingBeaconsInRegion(region.uuid);
            Beacons
                .startMonitoringForRegion(region) // minor and major are null here
                .then(() => {
                        //alert('Beacons monitoring started succesfully');
                        console.log('Beacons monitoring started succesfully')
                    }
                )
                .catch(error =>{
                        //alert('Beacons monitoring not started');
                        console.log('Beacons monitoring not started')
                    }
                );


        }

    }



    startRanging(){
        // Ranging: Listen for beacon changes
        this.beaconsDidRange = DeviceEventEmitter.addListener(
            'beaconsDidRange',
            (data) => {

                let count = data.beacons.length;
                if(count > 0) {

                    for(var i = 0; i < BeaconData.length; i++) {
                        for(var j = 0; j < data.beacons.length; j++) {
                            if(BeaconData[i].UUID.toLowerCase() == data.beacons[j].uuid.toLowerCase()){
                                if(BeaconData[i].Major == data.beacons[j].major.toString()){
                                    if(BeaconData[i].Minor == data.beacons[j].minor.toString()){

                                        this.setState({
                                            uuid:BeaconData[i].UUID,
                                            Major:BeaconData[i].Major,
                                            Minor:BeaconData[i].Minor,
                                            welcomeMsg:BeaconData[i].WelcomeMessage,
                                            delayTime:BeaconData[i].DelayTime,
                                            checkInTime:BeaconData[i].CheckInTime
                                        })

                                        return AsyncStorage.getItem('checkInTime', (err, result) => {

                                            if(result == null || result!=BeaconData[i].CheckInTime ) {
                                                return AsyncStorage.setItem('checkInTime',BeaconData[i].CheckInTime,()=>{

                                                }).then(()=>{
                                                    return AsyncStorage.removeItem('newSession')
                                                }).then(()=>{
                                                    return AsyncStorage.removeItem('connectionStart')
                                                })

                                            }

                                        }).then(()=>{

                                            this.newSession().then(res => {

                                                AsyncStorage.getItem('newSession', (err, result) => {
                                                    let session = JSON.parse(result)


                                                    if(session || session == null){

                                                        AsyncStorage.setItem('newSession', JSON.stringify(false));

                                                        if(this.state.welcomeMsg != '') {
                                                            this.showLocalNotification();
                                                        }

                                                    }

                                                    if(!session) {

                                                        AsyncStorage.getItem('connectionStart', (err, result) => {
                                                            let connection = JSON.parse(result);

                                                            if(!connection || connection == null){

                                                                if (!this.state.timer) {
                                                                    Moment.locale('en');
                                                                    let dt = new Date();

                                                                    try {
                                                                        AsyncStorage.setItem('InTime',dt.toString());
                                                                        this.setState({timer: true})
                                                                    } catch (error) {
                                                                    }

                                                                }

                                                                if(!delay) {

                                                                    AsyncStorage.getItem('InTime', (err, result1) => {

                                                                        let date = new Date(result1)
                                                                        intime = Moment(date)

                                                                        let currenttime = new Date();
                                                                        outtime = Moment(currenttime);

                                                                        let diffM = outtime.diff(intime, 'seconds');
                                                                        let delay1 = Moment.duration(this.state.delayTime, "HH:mm").asSeconds();

                                                                        if (diffM > delay1) {
                                                                            this.createHubConnection()
                                                                            AsyncStorage.setItem('connectionStart',JSON.stringify(true));
                                                                        }

                                                                    });


                                                                }

                                                            }



                                                        });

                                                    }
                                                });

                                            })
                                        })




                                    }
                                }
                            }
                        }
                    }



                }

                else{


                    if(this.state.timer){

                        this.setState({timer: false});
                        delay = false;
                        Moment.locale('en');
                        let dt = new Date();


                        try {

                            AsyncStorage.setItem('OutTime', Moment(dt));
                        } catch (error) {
                        }

                    }

                }

            }
        );

    }

    showAlert(){
        if(isHomePage){
            if(this.state.bluetoothState == 'Off'){
                Alert.alert('','Turn on your device Bluetooth to get points.',[{text: 'OK'},],{ cancelable: false });
            }
        }
    }


    async checkInitialBluetoothState() {
        const isEnabled = await BluetoothStatus.state();
        this.setState({ bluetoothState: (isEnabled) ? 'On' : 'Off'});
    }



    componentWillMount(){

        this.beaconConfig()

        FCM.requestPermissions();

        FCM.getInitialNotification().then(notif => {
            this.setState({
                initNotif: notif
            })
        });

    }


    componentDidMount(){

        this.checkInitialBluetoothState();
        isBackPressed=false
        BackHandler.addEventListener('hardwareBackPress', function() {
            // this.onMainScreen and this.goBack are just examples, you need to use your own implementation here
            // // Typically you would use the navigator here to go to the last state
            // if (that.state.themeType === "Buffalo" || that.state.themeType === "TGI_Fridays" || that.state.themeType === "Hulu" ){
            //     DeviceEventEmitter.emit('your listener',  {});
            //     return false;
            // }else{
            if(!isBackPressed)
            {
                ToastAndroid.show('Press back again to leave the app', ToastAndroid.SHORT);
                isBackPressed=true;
                return true

            }else{
                BackHandler.exitApp()
            }
            //
            //}
        });

    }

    render() {

        return (
            <View style={styles.container}>
                <WebView
                    onNavigationStateChange={this._onNavigationStateChange}
                    automaticallyAdjustContentInsets={false}
                    source={{uri: this.state.url}}
                    style={{marginTop:Platform.OS === 'ios' ? 20 : 0,flex:1,width: Dimensions.get('window').width, height: this.state.webViewHeight}}
                    onLoadStart={() => {
                        console.log("Loading start");
                        this.setAnimating(true);
                    }}
                    onLoadEnd={()=> {
                        console.log("Loading end");
                        this.setAnimating(false);
                    }}

                />
                <Loader backGroundImage={this.state.bgImage} animating={this.state.isAnimating} />
            </View>
        );
    }

    _onNavigationStateChange=(webViewState)=>{

        const {goBack} = this.props.navigation;
        AsyncStorage.setItem('lastURL', webViewState.url);

        if(webViewState.url=="https://alpha.roborewards.net/UserProfile/Login.aspx?Action=Logout")
        {
            if ( this.state.themeType === "Buffalo"||this.state.themeType.ThemeType === "Buffalo" || this.state.themeType === "TGI_Fridays"||this.state.themeType.ThemeType === "TGI_Fridays"  || this.state.themeType.ThemeType === "Hulu" ){
                this.setAnimating(false);
                this.setState({
                    isDirectWeb: false
                });
                goBack();
                DeviceEventEmitter.emit('your listener',  {});
            }
            isHomePage = false;
        }
        else if((webViewState.url).includes("contactid")){
            let url = webViewState.url
            var arr = url.split('=')
            if(arr.length > 2){
                this.setState({
                    contacId:arr[2]
                })
            }
            isHomePage = true;
            this.startRanging()
            this.showAlert()
        }else{
            isHomePage = false;
        }

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'transparent'
    },
});
