import { StackNavigator } from 'react-navigation';
import app from './app'
import webView from './Webview'
import buildStyleInterpolator from 'buildStyleInterpolator';
// const routes = StackNavigator({
//     SignIn: { screen: SignIn },
//     WebView: {screen: WebView}
// });

let TransitionConfiguration =
    (route) => {
        return {...Navigator.SceneConfigs.FloatFromRight, gestures:route.gestures };
    };

var NoTransition = {
    opacity: {
        value: 0.0,
        type: 'constant',
    }
};


const routes = StackNavigator({
    SignIn: { screen : app},
    WebView: { screen : webView}

},{
    headerMode: 'none'

},{
    transitionConfig: TransitionConfiguration,
    defaultTransitionVelocity: 1000,
    animationInterpolators: {
        into: buildStyleInterpolator(NoTransition),
        out: buildStyleInterpolator(NoTransition),
    },
});
module.exports = routes;