Object.defineProperty(exports, "__esModule", {
    value: true
});
var _jsxFileName = '/Users/itilak/Desktop/Projects/RoboRewards/Views/loginBuffalo.js';

var _react = require('react');

var _react2 = babelHelpers.interopRequireDefault(_react);

var _reactNative = require('react-native');

var _ApiCall = require('../Services/ApiCall');

var _loader = require('./loader');

var _loader2 = babelHelpers.interopRequireDefault(_loader);

var LoginBuffalo = function (_Component) {
    babelHelpers.inherits(LoginBuffalo, _Component);

    function LoginBuffalo(props) {
        babelHelpers.classCallCheck(this, LoginBuffalo);

        var _this = babelHelpers.possibleConstructorReturn(this, (LoginBuffalo.__proto__ || Object.getPrototypeOf(LoginBuffalo)).call(this, props));

        _this.setAnimating = function (flag) {
            _this.setState(function (prevState, props) {
                return { isAnimating: flag };
            });
        };

        _this.validate = function () {
            var flag = false;
            if (_this.state.email.length <= 0 || _this.state.pass.length <= 0) {
                _this._showAlert("Email Or Password can not be null.");
            } else {

                if (!_this.validateEmail(_this.state.email)) {

                    _this._showAlert("Enter valid email address.");
                } else {
                    flag = true;
                }
            }
            return flag;
        };

        _this.validateEmail = function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@â€œ]+(\.[^<>()\[\]\\.,;:\s@â€œ]+)*)|(â€œ.+â€œ))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        };

        _this._onPressSubmit = function () {
            var obj = {
                email: _this.state.email,
                pass: _this.state.pass,
                profile: "Buff",
                type: "login"
            };

            _this.setAnimating(true);

            if (_this.validate()) {
                (0, _ApiCall.commonAPICall)(obj).then(function (response) {
                    debugger;
                    console.log(response);
                    _this.setAnimating(false);

                    if (response.StatusCode == 1) {
                        var objUser = {
                            username: _this.state.email,
                            password: _this.state.pass,
                            Remembered: _this.state.isRemembered,
                            theme: _this.state.themeType
                        };
                        try {
                            _reactNative.AsyncStorage.setItem('User', JSON.stringify(objUser));
                        } catch (error) {}

                        var navigate = _this.props.navigation.navigate;

                        navigate('WebView', {
                            url: response.WebURL,
                            theme: "Buffalo"
                        });
                        _this.setState({
                            isFromWeb: true
                        });
                    } else {
                        _this._showAlert("Check Username and Password.");
                    }
                }).catch(function (error) {
                    _this.setAnimating(false);
                    console.log(error);
                });
            } else {
                _this.setAnimating(false);
            }
        };

        _this._onPressSignUp = function () {
            var obj = {
                profile: "Buff",
                type: "signup"
            };
            _this.setAnimating(true);
            (0, _ApiCall.commonAPICall)(obj).then(function (response) {
                _this.setAnimating(false);

                if (response.StatusCode == 1) {
                    console.log("SignUP://////", response.WebURL);

                    var navigate = _this.props.navigation.navigate;

                    navigate('WebView', { url: response.WebURL,
                        theme: "Buffalo" });
                } else {}
            }).catch(function (error) {
                _this.setAnimating(false);
                console.log(error);
            });
        };

        _this._onPressRememberMe = function () {
            _this.setState({
                isRemembered: !_this.state.isRemembered
            });
        };

        _this.state = {
            email: '',
            pass: '',
            isAnimating: false,
            backgroundImage: props.theme.BackgroundImageVideo,
            logoImage: props.theme.LogoImage,
            bannerDesColor: props.theme.BannerDescColor,
            bannerTitleColor: props.theme.BannerTitleColor,
            bgColor: props.theme.BgColor,
            bgOpacity: props.theme.BgOpacity,
            themeType: props.theme.ThemeType,
            isRemembered: false,
            isFromWeb: false
        };
        return _this;
    }

    babelHelpers.createClass(LoginBuffalo, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            var _this2 = this;

            try {
                _reactNative.AsyncStorage.getItem('User', function (err, data) {
                    if (data != null) {
                        var user = JSON.parse(data);
                        if (user.Remembered) {
                            _this2.setState(function (prevState, props) {
                                return {
                                    email: user.username,
                                    pass: user.password,
                                    isRemembered: user.Remembered
                                };
                            });
                        } else {
                            _this2.setState(function (prevState, props) {
                                return {
                                    email: "",
                                    pass: "",
                                    isRemembered: false
                                };
                            });
                        }
                    }
                });
            } catch (error) {}
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate() {
            var _this3 = this;

            if (this.state.isFromWeb) {
                try {
                    _reactNative.AsyncStorage.getItem('User', function (err, data) {
                        if (data != null) {
                            var user = JSON.parse(data);
                            if (user.Remembered) {
                                _this3.setState(function (prevState, props) {
                                    return {
                                        email: user.username,
                                        pass: user.password
                                    };
                                });
                            } else {
                                _this3.setState(function (prevState, props) {
                                    return {
                                        email: "",
                                        pass: "",
                                        isFromWeb: false
                                    };
                                });
                            }
                        }
                    });
                } catch (error) {}
            }
        }
    }, {
        key: '_showAlert',
        value: function _showAlert(textToMsg) {
            _reactNative.Alert.alert('', textToMsg, [{ text: 'OK', onPress: function onPress() {
                    return console.log('OK Pressed');
                } }], { cancelable: false });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this4 = this;

            return _react2.default.createElement(
                _reactNative.View,
                { style: [styles.container, { backgroundColor: this.state.bgColor != "" ? this.state.bgColor : 'white' }], __source: {
                        fileName: _jsxFileName,
                        lineNumber: 234
                    }
                },
                _react2.default.createElement(
                    _reactNative.View,
                    { style: { position: 'absolute', opacity: 0.9, top: 0, left: 0, right: 0, bottom: 0, backgroundColor: 'transparent' }, __source: {
                            fileName: _jsxFileName,
                            lineNumber: 235
                        }
                    },
                    _react2.default.createElement(_reactNative.Image, { style: { height: null, width: null, flex: 1 },
                        source: { uri: this.state.backgroundImage },
                        resizeMode: 'contain', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 236
                        }
                    })
                ),
                _react2.default.createElement(
                    _reactNative.View,
                    { style: { justifyContent: 'center', marginTop: 30, alignItems: 'center' }, __source: {
                            fileName: _jsxFileName,
                            lineNumber: 240
                        }
                    },
                    _react2.default.createElement(
                        _reactNative.View,
                        {
                            __source: {
                                fileName: _jsxFileName,
                                lineNumber: 241
                            }
                        },
                        _react2.default.createElement(
                            _reactNative.View,
                            { style: { justifyContent: 'flex-start', alignItems: 'center' }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 242
                                }
                            },
                            _react2.default.createElement(
                                _reactNative.Text,
                                { style: { color: this.state.bannerTitleColor != "" ? this.state.bannerTitleColor : 'black', fontSize: 30, fontWeight: '400', fontFamily: 'Aachen', backgroundColor: 'transparent' }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 243
                                    }
                                },
                                'ROBO REWARDS'
                            ),
                            _react2.default.createElement(_reactNative.Image, { style: { height: 100, width: 65, marginTop: 20 }, resizeMode: 'contain', source: { uri: this.state.logoImage }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 244
                                }
                            })
                        )
                    )
                ),
                _react2.default.createElement(
                    _reactNative.View,
                    { style: { padding: 30 }, __source: {
                            fileName: _jsxFileName,
                            lineNumber: 249
                        }
                    },
                    _react2.default.createElement(
                        _reactNative.View,
                        { style: { marginTop: 5, padding: 5 }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 250
                            }
                        },
                        _react2.default.createElement(
                            _reactNative.View,
                            { style: { flexDirection: 'row', height: 40, backgroundColor: 'rgba(255,255,255,0.7)' }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 251
                                }
                            },
                            _react2.default.createElement(_reactNative.TextInput, {
                                style: { flex: 1, justifyContent: 'flex-end', fontFamily: 'Aachen' },
                                onChangeText: function onChangeText(text) {
                                    return _this4.setState({ email: text });
                                },
                                placeholder: 'Email',
                                value: this.state.email,
                                underlineColorAndroid: 'transparent',
                                __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 252
                                }
                            })
                        ),
                        _react2.default.createElement(_reactNative.View, { style: { height: 1, backgroundColor: 'black' }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 261
                            }
                        })
                    ),
                    _react2.default.createElement(
                        _reactNative.View,
                        { style: { marginTop: 5, padding: 5 }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 264
                            }
                        },
                        _react2.default.createElement(
                            _reactNative.View,
                            { style: { flexDirection: 'row', height: 40, backgroundColor: 'rgba(255,255,255,0.7)' }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 265
                                }
                            },
                            _react2.default.createElement(_reactNative.TextInput, {
                                style: { flex: 1, justifyContent: 'flex-end', fontFamily: 'Aachen' },
                                onChangeText: function onChangeText(text) {
                                    return _this4.setState({ pass: text.toString() });
                                },
                                placeholder: 'password',
                                value: this.state.pass,
                                secureTextEntry: true,
                                underlineColorAndroid: 'transparent',
                                __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 266
                                }
                            })
                        ),
                        _react2.default.createElement(_reactNative.View, { style: { height: 1, backgroundColor: 'black' }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 275
                            }
                        })
                    ),
                    _react2.default.createElement(
                        _reactNative.View,
                        { style: { flexDirection: 'row', alignItems: 'center' }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 277
                            }
                        },
                        _react2.default.createElement(
                            _reactNative.TouchableOpacity,
                            { style: { height: 25, width: 25, marginTop: 10, borderColor: this.state.bannerDesColor, borderWidth: 0.5 },
                                onPress: function onPress() {
                                    return _this4._onPressRememberMe();
                                }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 278
                                }
                            },
                            _react2.default.createElement(_reactNative.Image, { style: { height: null, width: null, flex: 1, tintColor: this.state.bannerDesColor }, resizeMode: 'contain',
                                source: this.state.isRemembered ? require('./../Assets/Checked.png') : null, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 280
                                }
                            })
                        ),
                        _react2.default.createElement(
                            _reactNative.Text,
                            { style: { marginLeft: 10, marginTop: 7, backgroundColor: 'transparent', color: this.state.bannerDesColor }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 283
                                }
                            },
                            'Remember me'
                        )
                    ),
                    _react2.default.createElement(
                        _reactNative.View,
                        { style: { justifyContent: 'center', marginTop: 25 }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 285
                            }
                        },
                        _react2.default.createElement(
                            _reactNative.TouchableOpacity,
                            { onPress: function onPress() {
                                    return _this4._onPressSubmit();
                                }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 286
                                }
                            },
                            _react2.default.createElement(
                                _reactNative.View,
                                { style: { backgroundColor: '#E1E1E1', alignItems: 'center', height: 45, justifyContent: 'center' }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 287
                                    }
                                },
                                _react2.default.createElement(
                                    _reactNative.Text,
                                    { style: { color: '#9A9A99', fontSize: 20, fontWeight: '600', fontFamily: 'Aachen' }, __source: {
                                            fileName: _jsxFileName,
                                            lineNumber: 288
                                        }
                                    },
                                    'SIGN IN'
                                )
                            )
                        )
                    ),
                    _react2.default.createElement(
                        _reactNative.View,
                        { style: { justifyContent: 'center', marginTop: 15 }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 292
                            }
                        },
                        _react2.default.createElement(
                            _reactNative.TouchableOpacity,
                            {
                                __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 293
                                }
                            },
                            _react2.default.createElement(
                                _reactNative.View,
                                { style: { backgroundColor: 'white', alignItems: 'center', height: 45, justifyContent: 'center' }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 294
                                    }
                                },
                                _react2.default.createElement(
                                    _reactNative.Text,
                                    { style: { color: 'black', fontSize: 13, fontFamily: 'Aachen' }, __source: {
                                            fileName: _jsxFileName,
                                            lineNumber: 295
                                        }
                                    },
                                    'Forgot Password?'
                                )
                            )
                        )
                    ),
                    _react2.default.createElement(_reactNative.View, { style: { height: 2, borderRadius: 5, backgroundColor: '#E1E1E1' }, __source: {
                            fileName: _jsxFileName,
                            lineNumber: 299
                        }
                    }),
                    _react2.default.createElement(
                        _reactNative.View,
                        { style: { justifyContent: 'center', marginTop: 15 }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 300
                            }
                        },
                        _react2.default.createElement(
                            _reactNative.TouchableOpacity,
                            {
                                __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 301
                                }
                            },
                            _react2.default.createElement(
                                _reactNative.View,
                                { style: { alignItems: 'center', height: 45, justifyContent: 'center' }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 302
                                    }
                                },
                                _react2.default.createElement(
                                    _reactNative.Text,
                                    { style: { color: 'black', fontSize: 15, fontFamily: 'Aachen' }, __source: {
                                            fileName: _jsxFileName,
                                            lineNumber: 303
                                        }
                                    },
                                    'NOT A MEMBER YET?'
                                )
                            )
                        )
                    ),
                    _react2.default.createElement(
                        _reactNative.View,
                        { style: { justifyContent: 'center', marginBottom: 10 }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 307
                            }
                        },
                        _react2.default.createElement(
                            _reactNative.TouchableOpacity,
                            { onPress: function onPress() {
                                    return _this4._onPressSignUp();
                                }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 308
                                }
                            },
                            _react2.default.createElement(
                                _reactNative.View,
                                { style: { backgroundColor: '#FECA0A', alignItems: 'center', height: 45, marginBottom: 10, justifyContent: 'center' }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 309
                                    }
                                },
                                _react2.default.createElement(
                                    _reactNative.Text,
                                    { style: { color: 'black', fontSize: 15, fontFamily: 'Aachen', alignSelf: 'center' }, __source: {
                                            fileName: _jsxFileName,
                                            lineNumber: 310
                                        }
                                    },
                                    'JOIN NOW'
                                )
                            )
                        )
                    )
                ),
                _react2.default.createElement(_loader2.default, { animating: this.state.isAnimating, __source: {
                        fileName: _jsxFileName,
                        lineNumber: 315
                    }
                })
            );
        }
    }]);
    return LoginBuffalo;
}(_react.Component);

exports.default = LoginBuffalo;


var styles = _reactNative.StyleSheet.create({
    container: {
        flex: 1
    }
});