Object.defineProperty(exports, "__esModule", {
    value: true
});
var _jsxFileName = '/Users/itilak/Desktop/Projects/RoboRewards/Views/Webview.js';

var _react = require('react');

var _react2 = babelHelpers.interopRequireDefault(_react);

var _reactNative = require('react-native');

var _reactNativeVideo = require('react-native-video');

var _reactNativeVideo2 = babelHelpers.interopRequireDefault(_reactNativeVideo);

var _reactNavigation = require('react-navigation');

var _loader = require('./loader');

var _loader2 = babelHelpers.interopRequireDefault(_loader);

var window = _reactNative.Dimensions.get('window');

var flag = true;

var Webview = function (_Component) {
    babelHelpers.inherits(Webview, _Component);

    function Webview(props) {
        babelHelpers.classCallCheck(this, Webview);

        var _this = babelHelpers.possibleConstructorReturn(this, (Webview.__proto__ || Object.getPrototypeOf(Webview)).call(this, props));

        _this.setAnimating = function (flag) {
            _this.setState(function (prevState, props) {
                return { isAnimating: flag };
            });
        };

        _this._onNavigationStateChange = function (webViewState) {
            var goBack = _this.props.navigation.goBack;

            _reactNative.AsyncStorage.setItem('lastURL', webViewState.url);

            if (webViewState.url == "https://alpha.roborewards.net/UserProfile/Login.aspx?Action=Logout") {
                debugger;
                if (_this.state.themeType === "Buffalo" || _this.state.themeType.ThemeType === "Buffalo" || _this.state.themeType === "TGI_Fridays" || _this.state.themeType.ThemeType === "Hulu") {
                    _this.setAnimating(false);
                    _this.setState({
                        isDirectWeb: false
                    });
                    goBack();
                    _reactNative.DeviceEventEmitter.emit('your listener', {});
                }
            }
        };

        var state = _this.props.navigation.state;

        _this.state = {
            isAnimating: false,
            themeType: state.params.theme,
            url: state.params.url,
            isfromWeb: false
        };
        return _this;
    }

    babelHelpers.createClass(Webview, [{
        key: 'componentDidMount',
        value: function componentDidMount() {

            var that = this;
            _reactNative.BackHandler.addEventListener('hardwareBackPress', function () {
                if (that.state.themeType === "Buffalo" || that.state.themeType === "TGI_Fridays" || that.state.themeType === "Hulu") {
                    _reactNative.DeviceEventEmitter.emit('your listener', {});
                    return false;
                } else {
                    _reactNative.BackHandler.exitApp();
                }
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            return _react2.default.createElement(
                _reactNative.View,
                { style: styles.container, __source: {
                        fileName: _jsxFileName,
                        lineNumber: 59
                    }
                },
                _react2.default.createElement(_reactNative.WebView, {
                    onNavigationStateChange: this._onNavigationStateChange,
                    automaticallyAdjustContentInsets: false,
                    source: { uri: this.state.url },
                    style: { marginTop: 0, flex: 1, width: _reactNative.Dimensions.get('window').width, height: this.state.webViewHeight },
                    onLoadStart: function onLoadStart() {
                        console.log("Loading start");
                        _this2.setAnimating(true);
                    },
                    onLoadEnd: function onLoadEnd() {
                        console.log("Loading end");
                        _this2.setAnimating(false);
                    },

                    __source: {
                        fileName: _jsxFileName,
                        lineNumber: 60
                    }
                }),
                _react2.default.createElement(_loader2.default, { animating: this.state.isAnimating, __source: {
                        fileName: _jsxFileName,
                        lineNumber: 75
                    }
                })
            );
        }
    }]);
    return Webview;
}(_react.Component);

exports.default = Webview;


var styles = _reactNative.StyleSheet.create({
    container: {
        flex: 1
    }
});