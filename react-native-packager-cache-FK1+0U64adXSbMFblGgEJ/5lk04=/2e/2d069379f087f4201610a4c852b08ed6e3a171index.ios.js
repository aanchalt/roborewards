var _react = require('react');

var _react2 = babelHelpers.interopRequireDefault(_react);

var _reactNative = require('react-native');

var _Router = require('./Views/Router');

var _Router2 = babelHelpers.interopRequireDefault(_Router);

_reactNative.AppRegistry.registerComponent('RoboRewards', function () {
  return _Router2.default;
});