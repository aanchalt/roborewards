var _jsxFileName = '/Users/itilak/Desktop/Projects/RoboRewards/Views/FadeInView.js';

var _react = require('react');

var _react2 = babelHelpers.interopRequireDefault(_react);

var _reactNative = require('react-native');

var FadeInView = function (_Component) {
    babelHelpers.inherits(FadeInView, _Component);

    function FadeInView(props) {
        babelHelpers.classCallCheck(this, FadeInView);

        var _this = babelHelpers.possibleConstructorReturn(this, (FadeInView.__proto__ || Object.getPrototypeOf(FadeInView)).call(this, props));

        _this.state = {
            fadeAnim: new _reactNative.Animated.Value(0) };
        return _this;
    }

    babelHelpers.createClass(FadeInView, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            _reactNative.Animated.timing(this.state.fadeAnim, {
                toValue: 1,
                duration: 2000 }).start();
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                _reactNative.Animated.View,
                {
                    style: babelHelpers.extends({}, this.props.style, {
                        opacity: this.state.fadeAnim }),
                    __source: {
                        fileName: _jsxFileName,
                        lineNumber: 24
                    }
                },
                this.props.children
            );
        }
    }]);
    return FadeInView;
}(_react.Component);

module.exports = FadeInView;