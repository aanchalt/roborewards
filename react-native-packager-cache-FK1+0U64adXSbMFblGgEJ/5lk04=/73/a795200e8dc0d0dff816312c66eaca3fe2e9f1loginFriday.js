Object.defineProperty(exports, "__esModule", {
    value: true
});
var _jsxFileName = '/Users/itilak/Desktop/Projects/RoboRewards/Views/loginFriday.js';

var _react = require('react');

var _react2 = babelHelpers.interopRequireDefault(_react);

var _reactNative = require('react-native');

var _loader = require('./loader');

var _loader2 = babelHelpers.interopRequireDefault(_loader);

var _ApiCall = require('../Services/ApiCall');

var flag = false;

var LoginFriday = function (_Component) {
    babelHelpers.inherits(LoginFriday, _Component);

    function LoginFriday(props) {
        babelHelpers.classCallCheck(this, LoginFriday);

        var _this = babelHelpers.possibleConstructorReturn(this, (LoginFriday.__proto__ || Object.getPrototypeOf(LoginFriday)).call(this, props));

        _this.validate = function () {
            var flag = false;
            if (_this.state.email.length <= 0 || _this.state.pass.length <= 0) {
                _this._showAlert("Email Or Password can not be null.");
            } else {

                if (!_this.validateEmail(_this.state.email)) {

                    _this._showAlert("Enter valid email address.");
                } else {
                    flag = true;
                }
            }
            return flag;
        };

        _this.validateEmail = function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@â€œ]+(\.[^<>()\[\]\\.,;:\s@â€œ]+)*)|(â€œ.+â€œ))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        };

        _this.logInClicked = function () {
            _this.setState({
                DisplayLogin: true
            });
            _this.refs.scrollView.scrollToEnd(true);
        };

        _this._onPressSubmit = function () {
            _this.setAnimating(true);
            if (_this.validate()) {
                (0, _ApiCall.commonAPICall)({
                    email: _this.state.email,
                    pass: _this.state.pass,
                    profile: "Buff",
                    type: "login"
                }).then(function (response) {
                    console.log(response);
                    _this.setAnimating(false);
                    if (response.StatusCode == 1) {
                        var objUser = {
                            username: _this.state.email,
                            password: _this.state.pass,
                            Remembered: _this.state.isRemembered,
                            theme: _this.state.themeType
                        };
                        try {
                            _reactNative.AsyncStorage.setItem('User', JSON.stringify(objUser));
                        } catch (error) {}
                        var navigate = _this.props.navigation.navigate;

                        navigate('WebView', {
                            url: response.WebURL,
                            theme: "TGI_Fridays"
                        });
                        _this.setState({
                            isFromWeb: true
                        });
                    } else {
                        _this.setAnimating(false);
                        _this._showAlert("Check Username and Password.");
                    }
                }).catch(function (error) {
                    _this.setAnimating(false);
                    console.log(error);
                });
            } else {
                _this.setAnimating(false);
            }
        };

        _this._onPressSignUp = function () {

            _this.setAnimating(true);
            (0, _ApiCall.commonAPICall)({
                profile: "Buff",
                type: "signup"
            }).then(function (response) {
                _this.setAnimating(false);
                if (response.StatusCode == 1) {
                    var navigate = _this.props.navigation.navigate;

                    navigate('WebView', { url: response.WebURL,
                        theme: "TGI_Fridays" });
                } else {}
            }).catch(function (error) {
                _this.setAnimating(false);
            });
        };

        _this._onPressRememberMe = function () {
            _this.setState({
                isRemembered: !_this.state.isRemembered
            });
        };

        _this.setAnimating = function (flag) {

            _this.setState(function (prevState, props) {
                return { isAnimating: flag };
            });
        };

        _this.state = {
            email: '',
            pass: '',
            DisplayLogin: false,
            isAnimating: false,
            backgroundImage: props.theme.BackgroundImageVideo,
            logoImage: props.theme.LogoImage,
            bannerDesColor: props.theme.BannerDescColor,
            bannerTitleColor: props.theme.BannerTitleColor,
            bgColor: props.theme.BgColor,
            bgOpacity: props.theme.BgOpacity,
            themeType: props.theme.ThemeType,
            isRemembered: false,
            isFromWeb: false
        };
        return _this;
    }

    babelHelpers.createClass(LoginFriday, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            try {
                _reactNative.AsyncStorage.getItem('User', function (err, data) {
                    if (data != null) {
                        var user = JSON.parse(data);
                        if (user.Remembered) {
                            _this2.setState(function (prevState, props) {
                                return {
                                    email: user.username,
                                    pass: user.password,
                                    isRemembered: user.Remembered
                                };
                            });
                        }
                    }
                });
            } catch (error) {}
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate() {
            var _this3 = this;

            if (this.state.isFromWeb) {
                try {
                    _reactNative.AsyncStorage.getItem('User', function (err, data) {
                        if (data != null) {
                            var user = JSON.parse(data);
                            if (user.Remembered) {
                                _this3.setState(function (prevState, props) {
                                    return {
                                        email: user.username,
                                        pass: user.password
                                    };
                                });
                            } else {
                                _this3.setState(function (prevState, props) {
                                    return {
                                        email: "",
                                        pass: "",
                                        isFromWeb: false
                                    };
                                });
                            }
                        }
                    });
                } catch (error) {}
            }
        }
    }, {
        key: '_showAlert',
        value: function _showAlert(textToMsg) {
            _reactNative.Alert.alert('', textToMsg, [{ text: 'OK', onPress: function onPress() {
                    return console.log('OK Pressed');
                } }], { cancelable: false });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this4 = this;

            return _react2.default.createElement(
                _reactNative.View,
                { style: [styles.container, { backgroundColor: this.state.bgColor != "" ? this.state.bgColor : 'white' }], __source: {
                        fileName: _jsxFileName,
                        lineNumber: 231
                    }
                },
                _react2.default.createElement(
                    _reactNative.View,
                    { style: { position: 'absolute', opacity: 0.9, top: 0, left: 0, right: 0, bottom: 0, backgroundColor: 'transparent' }, __source: {
                            fileName: _jsxFileName,
                            lineNumber: 232
                        }
                    },
                    _react2.default.createElement(_reactNative.Image, { style: { height: null, width: null, flex: 1 },
                        blurRadius: 5,
                        source: { uri: this.state.backgroundImage },
                        resizeMode: 'contain', __source: {
                            fileName: _jsxFileName,
                            lineNumber: 233
                        }
                    })
                ),
                _react2.default.createElement(
                    _reactNative.ScrollView,
                    { ref: 'scrollView',
                        scrollEnabled: this.state.DisplayLogin,
                        onContentSizeChange: function onContentSizeChange(contentWidth, contentHeight) {
                            _scrollToBottomY = contentHeight;
                        }, __source: {
                            fileName: _jsxFileName,
                            lineNumber: 238
                        }
                    },
                    _react2.default.createElement(
                        _reactNative.View,
                        { style: { flex: 4, justifyContent: 'center', marginTop: 55, alignItems: 'center' }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 243
                            }
                        },
                        _react2.default.createElement(
                            _reactNative.View,
                            {
                                __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 244
                                }
                            },
                            _react2.default.createElement(
                                _reactNative.View,
                                { style: { flex: 1, justifyContent: 'center', alignItems: 'center' }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 245
                                    }
                                },
                                _react2.default.createElement(
                                    _reactNative.Text,
                                    { style: { color: this.state.bannerTitleColor != "" ? this.state.bannerTitleColor : 'black', fontSize: 30, fontWeight: '400', backgroundColor: 'transparent' }, __source: {
                                            fileName: _jsxFileName,
                                            lineNumber: 246
                                        }
                                    },
                                    'ROBOREWARDS'
                                )
                            ),
                            _react2.default.createElement(
                                _reactNative.View,
                                { style: { flex: 3, justifyContent: 'center', alignItems: 'center', padding: 15 }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 248
                                    }
                                },
                                _react2.default.createElement(_reactNative.View, { style: { height: 2, borderRadius: 5, backgroundColor: 'red', width: 100, marginBottom: 10 }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 249
                                    }
                                }),
                                _react2.default.createElement(
                                    _reactNative.View,
                                    {
                                        __source: {
                                            fileName: _jsxFileName,
                                            lineNumber: 250
                                        }
                                    },
                                    _react2.default.createElement(
                                        _reactNative.Text,
                                        { style: { color: this.state.bannerDesColor != "" ? this.state.bannerDesColor : 'black', fontSize: 20, fontWeight: '600', textAlign: 'center', backgroundColor: 'transparent' }, __source: {
                                                fileName: _jsxFileName,
                                                lineNumber: 251
                                            }
                                        },
                                        'OUR FRIEND GOES OUT BETTER'
                                    ),
                                    _react2.default.createElement(
                                        _reactNative.Text,
                                        { style: { color: this.state.bannerDesColor != "" ? this.state.bannerDesColor : 'black', fontSize: 20, fontWeight: '400', textAlign: 'center', backgroundColor: 'transparent' }, __source: {
                                                fileName: _jsxFileName,
                                                lineNumber: 252
                                            }
                                        },
                                        'WE HELP THEM GO OUT MORE'
                                    )
                                ),
                                _react2.default.createElement(_reactNative.View, { style: { height: 2, borderRadius: 5, backgroundColor: 'red', width: 100, margin: 10, alignItems: 'center' }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 254
                                    }
                                }),
                                _react2.default.createElement(
                                    _reactNative.Text,
                                    { style: { color: this.state.bannerDesColor != "" ? this.state.bannerDesColor : 'black', fontSize: 15, fontWeight: '400', textAlign: 'center', backgroundColor: 'transparent' }, __source: {
                                            fileName: _jsxFileName,
                                            lineNumber: 255
                                        }
                                    },
                                    'Become a rewards member and get perks no one else does.'
                                )
                            )
                        )
                    ),
                    _react2.default.createElement(
                        _reactNative.View,
                        { style: { flex: 2, alignItems: 'center', justifyContent: 'center', marginLeft: 70, marginRight: 70, marginTop: 30 }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 259
                            }
                        },
                        _react2.default.createElement(
                            _reactNative.TouchableOpacity,
                            { onPress: function onPress() {
                                    return _this4._onPressSignUp();
                                }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 260
                                }
                            },
                            _react2.default.createElement(
                                _reactNative.View,
                                { style: { backgroundColor: '#FF4754', width: 150, height: 45, alignItems: 'center', justifyContent: 'center', marginTop: 10 }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 261
                                    }
                                },
                                _react2.default.createElement(
                                    _reactNative.View,
                                    {
                                        __source: {
                                            fileName: _jsxFileName,
                                            lineNumber: 262
                                        }
                                    },
                                    _react2.default.createElement(
                                        _reactNative.Text,
                                        { style: { color: 'white', fontSize: 15, fontWeight: '400' }, __source: {
                                                fileName: _jsxFileName,
                                                lineNumber: 263
                                            }
                                        },
                                        'SIGN ME UP'
                                    )
                                )
                            )
                        ),
                        _react2.default.createElement(
                            _reactNative.TouchableOpacity,
                            { onPress: function onPress() {
                                    return _this4.logInClicked();
                                }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 267
                                }
                            },
                            _react2.default.createElement(
                                _reactNative.View,
                                { style: { backgroundColor: '#FF4754', width: 150, height: 45, alignItems: 'center', justifyContent: 'center', marginTop: 10 }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 268
                                    }
                                },
                                _react2.default.createElement(
                                    _reactNative.View,
                                    {
                                        __source: {
                                            fileName: _jsxFileName,
                                            lineNumber: 269
                                        }
                                    },
                                    _react2.default.createElement(
                                        _reactNative.Text,
                                        { style: { color: 'white', fontSize: 15, fontWeight: '400' }, __source: {
                                                fileName: _jsxFileName,
                                                lineNumber: 270
                                            }
                                        },
                                        'LOG IN'
                                    )
                                )
                            )
                        )
                    ),
                    this.state.DisplayLogin && _react2.default.createElement(
                        _reactNative.View,
                        { style: { flex: 1, height: 340, marginTop: 50, paddingLeft: 50, paddingRight: 50 }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 279
                            }
                        },
                        _react2.default.createElement(_reactNative.View, { style: { height: 3, borderRadius: 5, backgroundColor: 'white' }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 280
                            }
                        }),
                        _react2.default.createElement(
                            _reactNative.Text,
                            { style: { fontSize: 21, fontWeight: "700", color: 'white', textAlign: 'center', marginTop: 37, backgroundColor: 'transparent' }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 281
                                }
                            },
                            'WELCOME BACK!'
                        ),
                        _react2.default.createElement(_reactNative.TextInput, {
                            style: { height: 40, marginTop: 15, justifyContent: 'flex-end', backgroundColor: 'rgba(255,255,255,0.7)' },
                            onChangeText: function onChangeText(text) {
                                return _this4.setState({ email: text });
                            },
                            placeholder: 'email',
                            value: this.state.email,
                            underlineColorAndroid: 'transparent',
                            __source: {
                                fileName: _jsxFileName,
                                lineNumber: 283
                            }
                        }),
                        _react2.default.createElement(_reactNative.TextInput, {
                            style: { height: 40, marginTop: 10, justifyContent: 'flex-end', backgroundColor: 'rgba(255,255,255,0.7)' },
                            onChangeText: function onChangeText(text) {
                                return _this4.setState({ pass: text });
                            },
                            placeholder: 'password',
                            value: this.state.pass,
                            secureTextEntry: true,
                            underlineColorAndroid: 'transparent',
                            __source: {
                                fileName: _jsxFileName,
                                lineNumber: 290
                            }
                        }),
                        _react2.default.createElement(
                            _reactNative.View,
                            { style: { flexDirection: 'row', alignItems: 'center' }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 298
                                }
                            },
                            _react2.default.createElement(
                                _reactNative.TouchableOpacity,
                                { style: { height: 25, width: 25, marginTop: 10, borderColor: this.state.bannerDesColor, borderWidth: 0.5 },
                                    onPress: function onPress() {
                                        return _this4._onPressRememberMe();
                                    }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 299
                                    }
                                },
                                _react2.default.createElement(_reactNative.Image, { style: { height: null, width: null, flex: 1, tintColor: this.state.bannerDesColor },
                                    resizeMode: 'contain',
                                    source: this.state.isRemembered ? require('./../Assets/Checked.png') : null, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 301
                                    }
                                })
                            ),
                            _react2.default.createElement(
                                _reactNative.Text,
                                { style: { marginLeft: 10, marginTop: 7, backgroundColor: 'transparent', color: this.state.bannerDesColor }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 305
                                    }
                                },
                                'Remember me'
                            )
                        ),
                        _react2.default.createElement(
                            _reactNative.TouchableOpacity,
                            { style: { height: 60, marginTop: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: 'slategray' },
                                onPress: function onPress() {
                                    return _this4._onPressSubmit();
                                }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 308
                                }
                            },
                            _react2.default.createElement(
                                _reactNative.Text,
                                { style: { color: 'white', fontSize: 16, fontWeight: '500' }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 310
                                    }
                                },
                                'LOG IN'
                            )
                        ),
                        _react2.default.createElement(
                            _reactNative.TouchableOpacity,
                            { style: { height: 20, marginTop: 10, justifyContent: 'center', alignItems: 'center' }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 313
                                }
                            },
                            _react2.default.createElement(
                                _reactNative.Text,
                                { style: { color: 'white', fontSize: 11, fontWeight: '900', textDecorationLine: 'underline' }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 314
                                    }
                                },
                                'HAVING TROUBLE LOGGING IN?'
                            )
                        )
                    ) || _react2.default.createElement(_reactNative.View, { style: { flex: 1, height: 340, marginTop: 50, paddingLeft: 50, paddingRight: 50 }, __source: {
                            fileName: _jsxFileName,
                            lineNumber: 319
                        }
                    })
                ),
                _react2.default.createElement(_loader2.default, { animating: this.state.isAnimating, __source: {
                        fileName: _jsxFileName,
                        lineNumber: 323
                    }
                })
            );
        }
    }]);
    return LoginFriday;
}(_react.Component);

exports.default = LoginFriday;


var styles = _reactNative.StyleSheet.create({
    container: {
        flex: 1,

        backgroundColor: 'transparent'
    }
});