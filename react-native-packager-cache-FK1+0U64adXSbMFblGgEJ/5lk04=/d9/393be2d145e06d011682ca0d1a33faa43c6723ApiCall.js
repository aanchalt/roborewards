Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.commonAPICall = commonAPICall;

var _Helper = require("./Helper");

var _Helper2 = babelHelpers.interopRequireDefault(_Helper);

function commonAPICall(obj) {
    var strUrl = "";
    if (obj.profile == "Buff") {
        if (obj.type == "login") {
            strUrl = _Helper2.default.BaseURL + _Helper2.default.BuffUrl + _Helper2.default.BuffToken + "&UserName=" + obj.email + "&Password=" + obj.pass;
        } else if (obj.type == "signup") {
            strUrl = _Helper2.default.BaseURL + _Helper2.default.BuffUrlSignUp + _Helper2.default.BuffToken;
        }
    }
    if (obj.profile == "Hulu") {
        if (obj.type == "login") {
            strUrl = _Helper2.default.BaseURL + _Helper2.default.HuluUrl + _Helper2.default.HuluToken;
        } else if (obj.type == "signup") {
            strUrl = _Helper2.default.BaseURL + _Helper2.default.HuluUrlSignUp + _Helper2.default.HuluToken;
        }
    }
    if (obj.profile == "Initial") {
        strUrl = _Helper2.default.BaseURL + _Helper2.default.InitialTheme + _Helper2.default.InitialToken;
    }
    console.log(strUrl);

    return fetch(strUrl).then(function (response) {
        return response.json();
    }).then(function (responseJson) {
        return responseJson;
    }).catch(function (error) {
        console.error(error);
    });
}