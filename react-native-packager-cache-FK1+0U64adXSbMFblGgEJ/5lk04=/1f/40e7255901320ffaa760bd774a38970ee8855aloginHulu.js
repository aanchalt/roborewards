Object.defineProperty(exports, "__esModule", {
    value: true
});
var _jsxFileName = '/Users/itilak/Desktop/Projects/RoboRewards/Views/loginHulu.js';

var _react = require('react');

var _react2 = babelHelpers.interopRequireDefault(_react);

var _reactNative = require('react-native');

var _reactNativeVideo = require('react-native-video');

var _reactNativeVideo2 = babelHelpers.interopRequireDefault(_reactNativeVideo);

var _reactNavigation = require('react-navigation');

var _ApiCall = require('../Services/ApiCall');

var _loader = require('./loader');

var _loader2 = babelHelpers.interopRequireDefault(_loader);

var window = _reactNative.Dimensions.get('window');

var LoginHulu = function (_Component) {
    babelHelpers.inherits(LoginHulu, _Component);

    function LoginHulu(props) {
        babelHelpers.classCallCheck(this, LoginHulu);

        var _this = babelHelpers.possibleConstructorReturn(this, (LoginHulu.__proto__ || Object.getPrototypeOf(LoginHulu)).call(this, props));

        _this.setAnimating = function (flag) {
            _this.setState(function (prevState, props) {
                return { isAnimating: flag };
            });
        };

        _this._onPressSubmit = function () {
            var obj = {
                profile: "Hulu",
                type: "login"
            };
            _this.setAnimating(true);
            (0, _ApiCall.commonAPICall)(obj).then(function (response) {
                console.log(response);
                _this.setAnimating(false);

                if (response.StatusCode == 1) {
                    var navigate = _this.props.navigation.navigate;

                    navigate('WebView', { url: response.WebURL,
                        theme: "Hulu" });
                } else {
                    _this._showAlert("Check Username and Password.");
                }
            }).catch(function (error) {
                _this.setAnimating(false);
                console.log(response);
            });
        };

        _this._onPressSignUp = function () {
            var obj = {
                profile: "Hulu",
                type: "signup"
            };
            _this.setAnimating(true);
            (0, _ApiCall.commonAPICall)(obj).then(function (response) {
                _this.setAnimating(false);

                if (response.StatusCode == 1) {

                    var objUser = {
                        theme: _this.state.themeType
                    };
                    try {
                        _reactNative.AsyncStorage.setItem('User', JSON.stringify(objUser));
                    } catch (error) {}

                    var navigate = _this.props.navigation.navigate;

                    navigate('WebView', { url: response.WebURL,
                        theme: "Hulu" });
                } else {}
            }).catch(function (error) {
                _this.setAnimating(false);
                console.log(error);
            });
        };

        _this.state = {
            isAnimating: false,
            backgroundImage: props.theme.BackgroundImageVideo,
            logoImage: props.theme.LogoImage,
            bannerDesColor: props.theme.BannerDescColor,
            bannerTitleColor: props.theme.BannerTitleColor,
            bgColor: props.theme.BgColor,
            bgOpacity: props.theme.BgOpacity,
            themeType: props.theme.ThemeType

        };
        return _this;
    }

    babelHelpers.createClass(LoginHulu, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            var objUser = {
                theme: this.state.themeType
            };
            try {
                _reactNative.AsyncStorage.setItem('User', JSON.stringify(objUser));
            } catch (error) {}
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            return _react2.default.createElement(
                _reactNative.View,
                { style: [styles.container, { backgroundColor: this.state.bgColor != "" ? this.state.bgColor : 'white' }], __source: {
                        fileName: _jsxFileName,
                        lineNumber: 120
                    }
                },
                _react2.default.createElement(
                    _reactNative.View,
                    { style: { flex: 1 }, __source: {
                            fileName: _jsxFileName,
                            lineNumber: 121
                        }
                    },
                    _react2.default.createElement(_reactNativeVideo2.default, {

                        ref: function ref(_ref) {

                            _this2.player = _ref;
                        },

                        style: { height: window.height, width: window.width },

                        source: { uri: this.state.backgroundImage },

                        muted: true,

                        resizeMode: 'cover',
                        repeat: true,

                        __source: {
                            fileName: _jsxFileName,
                            lineNumber: 122
                        }
                    }),
                    _react2.default.createElement(
                        _reactNative.View,
                        { style: { position: 'absolute', top: 100, left: 0, bottom: 0, right: 0 }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 140
                            }
                        },
                        _react2.default.createElement(
                            _reactNative.View,
                            { style: { flex: 1 }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 141
                                }
                            },
                            _react2.default.createElement(
                                _reactNative.View,
                                { style: { flex: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent' }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 143
                                    }
                                },
                                _react2.default.createElement(
                                    _reactNative.Text,
                                    { style: { fontFamily: 'Cochin-Bold', fontSize: 40,
                                            fontWeight: '900', color: this.state.bannerTitleColor != "" ? this.state.bannerTitleColor : 'black' }, __source: {
                                            fileName: _jsxFileName,
                                            lineNumber: 144
                                        }
                                    },
                                    'RoboReward'
                                )
                            ),
                            _react2.default.createElement(
                                _reactNative.View,
                                { style: { flex: 3, padding: 15, justifyContent: 'flex-end', backgroundColor: 'transparent' }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 147
                                    }
                                },
                                _react2.default.createElement(
                                    _reactNative.Text,
                                    { style: { fontFamily: 'Cochin-Bold', fontSize: 30,
                                            fontWeight: '300', color: this.state.bannerDesColor != "" ? this.state.bannerDesColor : 'black' }, __source: {
                                            fileName: _jsxFileName,
                                            lineNumber: 148
                                        }
                                    },
                                    'RoboReward is a loyalty reward program.'
                                )
                            ),
                            _react2.default.createElement(
                                _reactNative.View,
                                { style: { flex: 3, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent' }, __source: {
                                        fileName: _jsxFileName,
                                        lineNumber: 151
                                    }
                                },
                                _react2.default.createElement(
                                    _reactNative.TouchableOpacity,
                                    { onPress: function onPress() {
                                            return _this2._onPressSignUp();
                                        }, __source: {
                                            fileName: _jsxFileName,
                                            lineNumber: 152
                                        }
                                    },
                                    _react2.default.createElement(
                                        _reactNative.View,
                                        { style: { backgroundColor: '#59AE42', padding: 10, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }, __source: {
                                                fileName: _jsxFileName,
                                                lineNumber: 153
                                            }
                                        },
                                        _react2.default.createElement(
                                            _reactNative.Text,
                                            { style: { fontSize: 15,
                                                    fontWeight: '400', color: this.state.bannerDesColor != "" ? this.state.bannerDesColor : 'black' }, __source: {
                                                    fileName: _jsxFileName,
                                                    lineNumber: 154
                                                }
                                            },
                                            'START YOUR FREE TRIAL'
                                        )
                                    )
                                ),
                                _react2.default.createElement(
                                    _reactNative.TouchableOpacity,
                                    { style: { marginTop: 10 }, onPress: function onPress() {
                                            return _this2._onPressSubmit();
                                        }, __source: {
                                            fileName: _jsxFileName,
                                            lineNumber: 158
                                        }
                                    },
                                    _react2.default.createElement(
                                        _reactNative.Text,
                                        { style: { fontSize: 15,
                                                fontWeight: 'normal', color: 'white' }, __source: {
                                                fileName: _jsxFileName,
                                                lineNumber: 160
                                            }
                                        },
                                        'log in'
                                    )
                                )
                            )
                        )
                    )
                ),
                _react2.default.createElement(_loader2.default, { animating: this.state.isAnimating, __source: {
                        fileName: _jsxFileName,
                        lineNumber: 171
                    }
                })
            );
        }
    }]);
    return LoginHulu;
}(_react.Component);

LoginHulu.navigationOptions = {
    tabBarLabel: 'LoginHulu'
};
exports.default = LoginHulu;


var styles = _reactNative.StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    }
});