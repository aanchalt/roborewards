Object.defineProperty(exports, "__esModule", {
    value: true
});
var _jsxFileName = '/Users/itilak/Desktop/Projects/RoboRewards/Views/app.js';

var _react = require('react');

var _react2 = babelHelpers.interopRequireDefault(_react);

var _reactNative = require('react-native');

var _loginHulu = require('./loginHulu');

var _loginHulu2 = babelHelpers.interopRequireDefault(_loginHulu);

var _loginFriday = require('./loginFriday');

var _loginFriday2 = babelHelpers.interopRequireDefault(_loginFriday);

var _Webview = require('./Webview');

var _Webview2 = babelHelpers.interopRequireDefault(_Webview);

var _loginBuffalo = require('./loginBuffalo');

var _loginBuffalo2 = babelHelpers.interopRequireDefault(_loginBuffalo);

var _loginJohn = require('./loginJohn');

var _loginJohn2 = babelHelpers.interopRequireDefault(_loginJohn);

var _loginSilverCity = require('./loginSilverCity');

var _loginSilverCity2 = babelHelpers.interopRequireDefault(_loginSilverCity);

var _ApiCall = require('../Services/ApiCall');

var _Helper = require('./../Services/Helper');

var _Helper2 = babelHelpers.interopRequireDefault(_Helper);

var themeNew = "";

var count = 0;

var App = function (_Component) {
    babelHelpers.inherits(App, _Component);

    function App(props) {
        babelHelpers.classCallCheck(this, App);

        var _this = babelHelpers.possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this));

        _this.getTheme = function () {

            if (_this.state.isDirectWeb) {
                if (themeNew.ThemeType != "") {
                    if (themeNew.ThemeType == "Buffalo") {
                        return _react2.default.createElement(_loginBuffalo2.default, {
                            theme: themeNew,
                            navigation: _this.props.navigation,
                            __source: {
                                fileName: _jsxFileName,
                                lineNumber: 129
                            }
                        });
                    } else if (themeNew.ThemeType == "TGI_Fridays") {
                        return _react2.default.createElement(_loginFriday2.default, {
                            theme: themeNew,
                            navigation: _this.props.navigation,
                            __source: {
                                fileName: _jsxFileName,
                                lineNumber: 137
                            }
                        });
                    } else if (themeNew.ThemeType == "Hulu") {
                        return _react2.default.createElement(_loginHulu2.default, {
                            theme: themeNew,
                            navigation: _this.props.navigation,
                            __source: {
                                fileName: _jsxFileName,
                                lineNumber: 145
                            }
                        });
                    } else if (themeNew.ThemeType == "Papa_Jones") {
                        return _react2.default.createElement(_loginJohn2.default, {
                            theme: themeNew,
                            navigation: _this.props.navigation,
                            __source: {
                                fileName: _jsxFileName,
                                lineNumber: 153
                            }
                        });
                    } else if (themeNew.ThemeType == "SilverCity_Rewards") {
                        return _react2.default.createElement(_loginSilverCity2.default, {
                            theme: themeNew,
                            navigation: _this.props.navigation,
                            __source: {
                                fileName: _jsxFileName,
                                lineNumber: 161
                            }
                        });
                    }
                }
            } else {
                return _react2.default.createElement(
                    _reactNative.View,
                    { style: { flex: 1, justifyContent: 'center', backgroundColor: 'rgb(1,26,49)' }, __source: {
                            fileName: _jsxFileName,
                            lineNumber: 170
                        }
                    },
                    _react2.default.createElement(_reactNative.Image, { style: { height: 50, width: 50, alignSelf: 'center' },
                        source: require('./../Assets/830.gif'), __source: {
                            fileName: _jsxFileName,
                            lineNumber: 171
                        }
                    })
                );
            }
        };

        _this.getWebView = function () {

            if (themeNew.ThemeType == "Buffalo" || themeNew.ThemeType == "TGI_Fridays") {
                _reactNative.AsyncStorage.getItem('User', function (err, result) {
                    if (result != null) {
                        var user = JSON.parse(result);
                        _this.getLogin({
                            email: user.username,
                            pass: user.password,
                            profile: "Buff",
                            type: "login"
                        });
                    } else {
                        debugger;
                        _this.setState({
                            isDirectWeb: true
                        });
                    }
                });
            } else {
                _this.setState({
                    isDirectWeb: true
                });
            }
        };

        _this.getLogin = function (credentials) {

            (0, _ApiCall.commonAPICall)(credentials).then(function (response) {

                if (response.StatusCode == 1) {
                    var navigate = _this.props.navigation.navigate;

                    navigate('WebView', { url: response.WebURL,
                        themeType: themeNew.ThemeType, theme: themeNew });
                } else {}
            }).catch(function (error) {});
        };

        _this.state = {
            theme: "",
            isDirectWeb: false,
            backGroundImageVideo: "",
            username: ''
        };
        return _this;
    }

    babelHelpers.createClass(App, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            var _this2 = this;

            _reactNative.DeviceEventEmitter.addListener('your listener', function (e) {
                _this2.setState({
                    isDirectWeb: true
                });
            });

            var obj = {
                profile: "Initial"
            };
            (0, _ApiCall.commonAPICall)(obj).then(function (response) {
                if (response.StatusCode == 1) {
                    themeNew = response;
                    debugger;
                    _reactNative.AsyncStorage.getItem('User', function (err, result) {
                        debugger;
                        if (result != null) {
                            var user = JSON.parse(result);
                            if (user.theme != themeNew.ThemeType) {
                                _this2.setState({
                                    isDirectWeb: true
                                });
                            } else {
                                _reactNative.AsyncStorage.getItem('lastURL', function (err, result1) {
                                    if (result1 != "https://alpha.roborewards.net/UserProfile/Login.aspx?Action=Logout") {
                                        _this2.getWebView();
                                    } else {
                                        _this2.setState({
                                            isDirectWeb: true
                                        });
                                    }
                                });
                            }
                        } else {
                            debugger;
                            _reactNative.AsyncStorage.getItem('lastURL', function (err, result1) {
                                if (result1 != "https://alpha.roborewards.net/UserProfile/Login.aspx?Action=Logout") {
                                    _this2.getWebView();
                                } else {
                                    _this2.setState({
                                        isDirectWeb: true
                                    });
                                }
                            });
                        }
                    });
                } else {
                    _this2._showAlert("Check Username and Password.");
                }
            }).catch(function (error) {
                return console.log(error);
            });
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                _reactNative.View,
                { style: styles.container, __source: {
                        fileName: _jsxFileName,
                        lineNumber: 231
                    }
                },
                _react2.default.createElement(_reactNative.StatusBar, {
                    hidden: false,
                    __source: {
                        fileName: _jsxFileName,
                        lineNumber: 232
                    }
                }),
                this.getTheme()
            );
        }
    }]);
    return App;
}(_react.Component);

exports.default = App;


var styles = _reactNative.StyleSheet.create({
    container: {
        flex: 1,

        backgroundColor: '#F5FCFF'
    }
});