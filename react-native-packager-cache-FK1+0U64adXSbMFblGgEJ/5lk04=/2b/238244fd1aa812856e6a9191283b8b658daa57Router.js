var _reactNavigation = require('react-navigation');

var _app = require('./app');

var _app2 = babelHelpers.interopRequireDefault(_app);

var _Webview = require('./Webview');

var _Webview2 = babelHelpers.interopRequireDefault(_Webview);

var routes = (0, _reactNavigation.StackNavigator)({
    SignIn: { screen: _app2.default },
    WebView: { screen: _Webview2.default }

}, {
    headerMode: 'none'

});
module.exports = routes;