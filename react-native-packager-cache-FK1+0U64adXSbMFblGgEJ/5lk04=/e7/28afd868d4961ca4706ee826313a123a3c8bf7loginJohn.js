Object.defineProperty(exports, "__esModule", {
    value: true
});
var _jsxFileName = '/Users/itilak/Desktop/Projects/RoboRewards/Views/loginJohn.js';

var _react = require('react');

var _react2 = babelHelpers.interopRequireDefault(_react);

var _reactNative = require('react-native');

var _ApiCall = require('../Services/ApiCall');

var LoginJohn = function (_Component) {
    babelHelpers.inherits(LoginJohn, _Component);

    function LoginJohn(props) {
        babelHelpers.classCallCheck(this, LoginJohn);

        var _this = babelHelpers.possibleConstructorReturn(this, (LoginJohn.__proto__ || Object.getPrototypeOf(LoginJohn)).call(this, props));

        _this.getNextScreen = function () {
            debugger;
            var obj = {
                profile: "Hulu",
                type: "signup"
            };
            (0, _ApiCall.commonAPICall)(obj).then(function (response) {
                console.log(response);
                if (response.StatusCode == 1) {

                    var that = _this;
                    setTimeout(function () {
                        var navigate = that.props.navigation.navigate;

                        navigate('WebView', { url: response.WebURL,
                            theme: "Papa_Jones" });
                    }, 3000);
                } else {
                    _this._showAlert("Check Username and Password.");
                }
            }).catch(function (error) {
                return console.log(response);
            });
        };

        _this.state = {
            backgroundImage: props.theme.BackgroundImageVideo,
            logoImage: props.theme.LogoImage,
            bannerDesColor: props.theme.BannerDescColor,
            bannerTitleColor: props.theme.BannerTitleColor,
            bgColor: props.theme.BgColor,
            bgOpacity: props.theme.BgOpacity,
            themeType: props.theme.ThemeType
        };
        return _this;
    }

    babelHelpers.createClass(LoginJohn, [{
        key: 'componentDidMount',
        value: function componentDidMount() {}
    }, {
        key: 'componentWillMount',
        value: function componentWillMount() {
            var objUser = {
                theme: this.state.themeType
            };
            try {
                _reactNative.AsyncStorage.setItem('User', JSON.stringify(objUser));
            } catch (error) {}
        }
    }, {
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                _reactNative.View,
                { style: styles.container, __source: {
                        fileName: _jsxFileName,
                        lineNumber: 70
                    }
                },
                _react2.default.createElement(
                    _reactNative.View,
                    { style: { opacity: 0.4, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, backgroundColor: 'transparent' }, __source: {
                            fileName: _jsxFileName,
                            lineNumber: 71
                        }
                    },
                    _react2.default.createElement(_reactNative.Image, { style: { height: null, width: null, flex: 1, opacity: 0.2 },
                        source: { uri: this.state.backgroundImage },
                        resizeMode: 'cover',
                        onLoadEnd: this.getNextScreen, __source: {
                            fileName: _jsxFileName,
                            lineNumber: 72
                        }
                    })
                ),
                _react2.default.createElement(
                    _reactNative.View,
                    { style: { flex: 1, marginTop: 55, justifyContent: 'center', backgroundColor: 'transparent' }, __source: {
                            fileName: _jsxFileName,
                            lineNumber: 77
                        }
                    },
                    _react2.default.createElement(
                        _reactNative.View,
                        { style: { flex: 2, justifyContent: 'center', marginTop: 55, alignItems: 'center' }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 78
                            }
                        },
                        _react2.default.createElement(
                            _reactNative.View,
                            {
                                __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 79
                                }
                            },
                            _react2.default.createElement(_reactNative.Image, { style: { height: 120, width: 120, backgroundColor: 'transparent' }, source: { uri: this.state.logoImage }, __source: {
                                    fileName: _jsxFileName,
                                    lineNumber: 80
                                }
                            })
                        )
                    ),
                    _react2.default.createElement(
                        _reactNative.View,
                        { style: { flex: 2, alignItems: 'center', justifyContent: 'center' }, __source: {
                                fileName: _jsxFileName,
                                lineNumber: 83
                            }
                        },
                        _react2.default.createElement(_reactNative.Image, { style: { height: 70, width: 70, backgroundColor: 'transparent' }, source: require('../Assets/loadingindicator.gif'), __source: {
                                fileName: _jsxFileName,
                                lineNumber: 84
                            }
                        })
                    ),
                    _react2.default.createElement(_reactNative.View, { style: { flex: 2 }, __source: {
                            fileName: _jsxFileName,
                            lineNumber: 86
                        }
                    })
                )
            );
        }
    }]);
    return LoginJohn;
}(_react.Component);

exports.default = LoginJohn;


var styles = _reactNative.StyleSheet.create({
    container: {
        flex: 1,

        backgroundColor: 'transparent'
    }
});