Object.defineProperty(exports, "__esModule", {
    value: true
});

var Helper = {

    BaseURL: "https://alpha.roborewards.net/robo/NewUserProfile/",

    BuffUrl: "ContactLogin?RewardProgramToken=",
    BuffToken: "USNo1wRUjql8MvH1QL8ga024m1J02m",
    BuffUrlSignUp: "RegisterWebFormURLByRPToken?RewardProgramToken=",

    HuluUrl: "LoginWebFormURLByRPToken?RewardProgramToken=",
    HuluToken: "USNo1wRUjql8MvH1QL8ga024m1J02m",
    HuluUrlSignUp: "RegisterWebFormURLByRPToken?RewardProgramToken=",

    InitialTheme: "ThemeLayoutByRPToken?RewardProgramToken=",
    InitialToken: "USNo1wRUjql8MvH1QL8ga024m1J02m"
};
exports.default = Helper;