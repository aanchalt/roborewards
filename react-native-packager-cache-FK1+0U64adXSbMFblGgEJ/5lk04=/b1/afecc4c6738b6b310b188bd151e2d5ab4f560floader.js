var _jsxFileName = '/Users/itilak/Desktop/Projects/RoboRewards/Views/loader.js';

var _react = require('react');

var _react2 = babelHelpers.interopRequireDefault(_react);

var _reactNative = require('react-native');

var Loader = function (_Component) {
    babelHelpers.inherits(Loader, _Component);

    function Loader() {
        babelHelpers.classCallCheck(this, Loader);
        return babelHelpers.possibleConstructorReturn(this, (Loader.__proto__ || Object.getPrototypeOf(Loader)).apply(this, arguments));
    }

    babelHelpers.createClass(Loader, [{
        key: 'render',
        value: function render() {
            return this.props.animating ? _react2.default.createElement(
                _reactNative.View,
                { style: { position: 'absolute', backgroundColor: 'rgba(0,0,0,0.5)',
                        height: _reactNative.Dimensions.get('window').height, width: _reactNative.Dimensions.get('window').width,
                        alignItems: 'center', justifyContent: 'center' }, __source: {
                        fileName: _jsxFileName,
                        lineNumber: 14
                    }
                },
                _react2.default.createElement(_reactNative.ActivityIndicator, {
                    animating: true,
                    size: 'large',
                    color: '#FFF',
                    __source: {
                        fileName: _jsxFileName,
                        lineNumber: 17
                    }
                })
            ) : null;
        }
    }]);
    return Loader;
}(_react.Component);

module.exports = Loader;